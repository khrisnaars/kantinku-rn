import * as React from 'react';
import {Root} from 'native-base';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

//SPLASH & INTRO
import splashScreen from './screens/startup/splashScreen';

//LOGIN
import loginScreen from './screens/login/loginScreen';

//HOME
import homeScreen from './screens/home/homeScreen';
import mutasiScreen from './screens/home/mutasiScreen';
import formMenuScreen from './screens/kantin/formMenuScreen'

//KANTIN
import kantinScreen from './screens/kantin/kantinScreen';
import menuScreen from './screens/kantin/menuScreen';

//HISTORY
import historyScreen from './screens/history/historyScreen';
import detailHistoryScreen from './screens/history/detailHistoryScreen';

const Stack = createStackNavigator();

function MainStackNavigator() {
  console.disableYellowBox = true;
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="splashScreen">
        <Stack.Screen
          name="splashScreen"
          component={splashScreen}
          options={{gestureEnabled: true, headerShown: false}}
        />
        <Stack.Screen
          name="loginScreen"
          component={loginScreen}
          options={{gestureEnabled: true, headerShown: false}}
        />
        <Stack.Screen
          name="homeScreen"
          component={homeScreen}
          options={{gestureEnabled: true, headerShown: false}}
        />
        <Stack.Screen
          name="kantinScreen"
          component={kantinScreen}
          options={{gestureEnabled: true, headerShown: false}}
        />
        <Stack.Screen
          name="mutasiScreen"
          component={mutasiScreen}
          options={{
            gestureEnabled: true,
            headerShown: true,
            title: 'Mutasi',
            headerStyle: {
              backgroundColor: '#BA1C33',
            },
            headerTintColor: '#FFF',
          }}
        />
        <Stack.Screen
          name="historyScreen"
          component={historyScreen}
          options={{
            gestureEnabled: true,
            headerShown: true,
            title: 'History',
            headerStyle: {
              backgroundColor: '#BA1C33',
            },
            headerTintColor: '#FFF',
          }}
        />
        <Stack.Screen
          name="detailHistoryScreen"
          component={detailHistoryScreen}
          options={{
            gestureEnabled: true,
            headerShown: true,
            title: 'Detail History',
            headerStyle: {
              backgroundColor: '#BA1C33',
            },
            headerTintColor: '#FFF',
          }}
        />
        <Stack.Screen
          name="menuScreen"
          component={menuScreen}
          options={{
            gestureEnabled: true,
            headerShown: true,
            title: 'List Menu',
            headerStyle: {
              backgroundColor: '#BA1C33',
            },
            headerTintColor: '#FFF',
          }}
        />
        <Stack.Screen
          name="formMenuScreen"
          component={formMenuScreen}
          options={{
            gestureEnabled: true,
            headerShown: true,
            title: 'Form Menu',
            headerStyle: {
              backgroundColor: '#BA1C33',
            },
            headerTintColor: '#FFF',
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default function App() {
  return (
    <Root>
      <MainStackNavigator />
    </Root>
  );
}
