import React, {Component} from 'react';
import {
  View,
  Text,
  Button,
  Item,
  Icon,
  Input,
  Container,
  Toast,
} from 'native-base';
import {
  Platform,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import axios from 'react-native-axios';
import Modal from 'react-native-modal';
import AsyncStorage from '@react-native-community/async-storage';
import {CommonActions} from '@react-navigation/native';
import Spinner from 'react-native-loading-spinner-overlay';

//DEVICE DIMENSION
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

//UTILS
import Logos from '../../utils/logos';
import Icons from '../../utils/icons';
import Colors from '../../utils/colors';
import GlobalVariables from '../../utils/globalVariables';

//CUSTOM COMPONENT
import ConfirmModal from '../modal/confirmModal';
import StatusBarWhite from '../statusBar/statusBarWhite';

//RESET NAVIGATION
const resetHome = CommonActions.reset({
  index: 1,
  routes: [{name: 'homeScreen'}],
});

const resetKantin = CommonActions.reset({
  index: 1,
  routes: [{name: 'kantinScreen'}],
});

class loginScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      nim: null,
      pin: null,
      isHiddenPassword: true,
      loading: false,
    };
  }

  //Tampilkan Modal Confirm
  toggleModalConfirm = () => {
    this.setState({isModalVisible: !this.state.isModalVisible});
  };

  //Next Field
  focusNextField = nextField => {
    this.refs[nextField].wrappedInstance.focus();
  };

  //Show Password
  toggleHiddenPassword() {
    this.setState({
      isHiddenPassword: !this.state.isHiddenPassword,
    });
  }

  //Set Api Login
  async getUserLogin() {
    let url = GlobalVariables.URL_KANTINKU + GlobalVariables.URL_POST_LOGIN;

    let parameter = {
      nim: this.state.nim,
      password: this.state.pin,
    };

    const res = await axios.post(url, parameter);

    return await res;
  }

  async getDataKantin() {
    let url =
      GlobalVariables.URL_KANTINKU + GlobalVariables.URL_POST_LOGIN_KANTIN;

    let parameter = {
      username: this.state.nim,
      password: this.state.pin,
    };

    const res = await axios.post(url, parameter);

    return await res;
  }

  async storeData(data, status) {
    try {
      if (status == 1) {
        var data = data;

        await AsyncStorage.setItem('dataUser', JSON.stringify(data));
        this.setState({loading: false}, () => {
          this.props.navigation.dispatch(resetHome);
        });
      } else {
        var data = data;

        await AsyncStorage.setItem('dataUser', JSON.stringify(data));
        this.setState({loading: false}, () => {
          this.props.navigation.dispatch(resetKantin);
        });
      }
    } catch (e) {
      this.setState({loading: false}, () => {
        Toast.show({
          text: 'Coba beberapa saat lagi kami gagal buat session untuk kamu',
          buttonText: 'X',
          type: 'danger',
          duration: 3000,
        });
      });
    }
  }

  //Login
  Login() {
    this.setState({loading: true}, () => {
      if (
        /^[a-zA-Z]+$/.test(this.state.nim) &&
        this.state.nim != null &&
        this.state.nim != ''
      ) {
        this.getDataKantin()
          .then(response => {
            response = response.data;
            if (response.status == 1) {
              this.storeData(response.pesan, 2);
            } else {
              this.setState({loading: false}, () => {
                Toast.show({
                  text: response.error,
                  buttonText: 'X',
                  type: 'danger',
                  duration: 3000,
                });
              });
            }
          })
          .catch(function(error) {
            this.setState({loading: false}, () => {
              Toast.show({
                text:
                  'Terjadi kesalahan pastikan koneksi anda baik, dan hubungi administrator',
                buttonText: 'X',
                type: 'danger',
                duration: 3000,
              });
            });
          });
      } else {
        this.getUserLogin()
          .then(response => {
            response = response.data;
            if (response.status == 1) {
              this.storeData(response.pesan, 1);
            } else {
              this.setState({loading: false}, () => {
                Toast.show({
                  text: response.error,
                  buttonText: 'X',
                  type: 'danger',
                  duration: 3000,
                });
              });
            }
          })
          .catch(function(error) {
            this.setState({loading: false}, () => {
              Toast.show({
                text:
                  'Terjadi kesalahan pastikan koneksi anda baik, dan hubungi administrator',
                buttonText: 'X',
                type: 'danger',
                duration: 3000,
              });
            });
          });
      }
    });
  }

  render() {
    return (
      <Container>
        <StatusBarWhite />

        <Spinner
          visible={this.state.loading}
          textContent={'Loading...'}
          textStyle={{color: Colors.COLOR_BLACK}}
        />

        <View style={Styles.logoContainer}>
          <Image source={Logos.logo_kantinku_01} style={Styles.logo} />
        </View>
        <View style={Styles.content}>
          <View style={[Styles.contentCard, Styles.shadow]}>
            <Item style={Styles.iconUserContainer}>
              <Icon
                type="SimpleLineIcons"
                name="user"
                style={Styles.iconForm}
              />
              <Input
                onSubmitEditing={() => this.focusNextField('pin')}
                returnKeyType={'next'}
                ref="nim"
                disabled={this.state.isDisabled}
                placeholder="Nim"
                onChangeText={text => this.setState({nim: text})}
              />
            </Item>
            <Item style={Styles.iconPassContainer}>
              <Icon
                type="SimpleLineIcons"
                name="lock"
                style={Styles.iconForm}
              />
              <Input
                secureTextEntry={this.state.isHiddenPassword}
                ref="pin"
                disabled={this.state.isDisabled}
                placeholder="Pin"
                onChangeText={text => this.setState({pin: text})}
                onSubmitEditing={() => this.Login()}
              />
              <TouchableOpacity
                onPress={() => {
                  this.toggleHiddenPassword();
                }}>
                {this.state.isHiddenPassword ? (
                  <Icon active name="ios-eye-off" />
                ) : (
                  <Icon active name="ios-eye" />
                )}
              </TouchableOpacity>
            </Item>
          </View>
          <View>
            <Button
              rounded
              full
              style={[Styles.btn, Styles.shadow]}
              onPress={() => this.Login()}
              disabled={this.state.isDisabled}>
              <Text>LOGIN</Text>
            </Button>
          </View>
        </View>
      </Container>
    );
  }
}

const Styles = StyleSheet.create({
  //Login
  logoContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1,
    marginBottom: 20,
  },
  content: {
    height: deviceHeight * 0.6,
    backgroundColor: Colors.COLOR_WHITE,
  },
  contentCard: {
    backgroundColor: Colors.COLOR_WHITE,
    paddingVertical: 10,
    paddingHorizontal: 20,
    margin: 15,
    borderRadius: 8,
    marginTop: -20,
  },
  shadow: {
    ...Platform.select({
      ios: {
        shadowOffset: {height: 1},
        shadowColor: 'grey',
        shadowOpacity: 0.5,
      },
      android: {
        elevation: 3,
      },
    }),
  },
  iconUserContainer: {
    padding: 5,
  },
  iconPassContainer: {
    borderColor: 'transparent',
    padding: 5,
  },
  iconForm: {
    fontSize: 18,
    color: Colors.COLOR_BLACK_47,
    marginHorizontal: 10,
  },
  btn: {
    backgroundColor: Colors.COLOR_PRIMARY,
    alignSelf: 'center',
    width: deviceWidth * 0.5,
    borderRadius: 24,
    marginTop: 10,
    padding: 20,
  },
  logo: {
    width: deviceWidth * 0.5,
    height: deviceWidth * 0.5,
    margin: '10%',
    resizeMode: 'contain',
  },
});

export default loginScreen;
