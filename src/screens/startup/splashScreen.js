import React, {Component} from 'react';
import {View} from 'native-base';
import {StyleSheet, Image, Dimensions, ImageBackground} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {CommonActions} from '@react-navigation/native';

//UTILS
import Logos from '../../utils/logos';
import Backgrounds from '../../utils/backgrounds';

//CUSTOM COMPONENT
import StatusBarWhite from '../statusBar/statusBarWhite';

//RESET NAVIGATION
const resetLogin = CommonActions.reset({
  index: 1,
  routes: [{name: 'loginScreen'}],
});

const resetHome = CommonActions.reset({
  index: 1,
  routes: [{name: 'homeScreen'}],
});

const resetKantin = CommonActions.reset({
  index: 1,
  routes: [{name: 'kantinScreen'}],
});

//DEVICE DIMENSION
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

class splashScreen extends Component {
  componentDidMount() {
    setTimeout(() => {
      AsyncStorage.getItem('isIntroShowed', (error, result) => {
        if (result) {
        } else {
          this.getData();
        }
      });
    }, 2 * 1000);
  }

  async getData() {
    var dataUser = await AsyncStorage.getItem('dataUser');

    dataUser = JSON.parse(dataUser);

    if (dataUser != null) {
      if (dataUser.USERNAME) {
        this.props.navigation.dispatch(resetKantin);
      } else {
        this.props.navigation.dispatch(resetHome);
      }
    } else {
      this.props.navigation.dispatch(resetLogin);
    }
  }

  render() {
    return (
      <View>
        <StatusBarWhite />
        <ImageBackground
          source={Backgrounds.background_kantinku_01}
          style={Styles.containerSplash}>
          <Image source={Logos.logo_kantinku_01} style={Styles.logoImage} />
        </ImageBackground>
      </View>
    );
  }
}

const Styles = StyleSheet.create({
  containerSplash: {
    width: deviceWidth,
    height: deviceHeight,
    resizeMode: 'contain',
  },
  logoImage: {
    width: deviceWidth * 0.5,
    height: deviceHeight * 0.5,
    resizeMode: 'contain',
    marginHorizontal: '25%',
    marginVertical: '50%',
  },
});

export default splashScreen;
