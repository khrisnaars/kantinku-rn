import React, {Component} from 'react';
import {View, Text, Button} from 'native-base';
import {Platform, StyleSheet, Dimensions} from 'react-native';
import Modal from 'react-native-modal';

//DEVICE DIMENSION
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

//UTILS
import Colors from '../../utils/colors';

class confirmModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isModalVisible: this.props.isVisible,
    };
  }

  render() {
    return (
      <Modal isVisible={this.state.isModalVisible}>
        <View style={Styles.modalContainer}>
          <View style={Styles.textContainer}>
            <Text style={Styles.titleModal}>Hanya boleh 1 device...</Text>
            <Text>
              Kami mendeteksi bahwa anda masih login pada device lain kami akan
              secara otomatis mengeluarkan account pada device tersebut,apakah
              anda ingin melanjutkan ?
            </Text>
          </View>
          <View style={Styles.btnModalContainer}>
            <View style={Styles.btn1ModalContainer}>
              <Button style={Styles.btn1} onPress={this.props.action}>
                <Text>Ya</Text>
              </Button>
            </View>
            <View style={Styles.btn2ModalContainer}>
              <Button
                style={Styles.btn2}
                onPress={() => this.setState({isModalVisible: false})}>
                <Text>Tidak</Text>
              </Button>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}
// function confirmModal({props}) {
//   console.log()
// }

const Styles = StyleSheet.create({
  //Login
  logoContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1,
    marginBottom: 20,
  },
  content: {
    height: deviceHeight * 0.6,
    backgroundColor: Colors.COLOR_WHITE,
  },
  contentCard: {
    backgroundColor: Colors.COLOR_WHITE,
    paddingVertical: 10,
    paddingHorizontal: 20,
    margin: 15,
    borderRadius: 8,
    marginTop: -20,
  },
  shadow: {
    ...Platform.select({
      ios: {
        shadowOffset: {height: 1},
        shadowColor: 'grey',
        shadowOpacity: 0.5,
      },
      android: {
        elevation: 3,
      },
    }),
  },
  iconUserContainer: {
    padding: 5,
  },
  iconPassContainer: {
    borderColor: 'transparent',
    padding: 5,
  },
  iconForm: {
    fontSize: 18,
    color: Colors.COLOR_BLACK_47,
    marginHorizontal: 10,
  },
  btn: {
    backgroundColor: Colors.COLOR_PRIMARY,
    alignSelf: 'center',
    width: deviceWidth * 0.5,
    borderRadius: 24,
    marginTop: 10,
    padding: 20,
  },
  logo: {
    width: deviceWidth * 0.5,
    height: deviceWidth * 0.5,
    margin: '10%',
    resizeMode: 'contain',
  },

  //Modal
  modalContainer: {
    height: deviceHeight * 0.3,
    backgroundColor: Colors.COLOR_WHITE,
    borderRadius: deviceWidth * 0.02,
  },
  textContainer: {
    margin: deviceWidth * 0.02,
    flex: 1,
  },
  titleModal: {
    marginBottom: deviceWidth * 0.02,
    fontSize: deviceWidth * 0.05,
  },
  btnModalContainer: {
    display: 'flex',
    flexDirection: 'row',
  },
  btn1ModalContainer: {
    flex: 1,
  },
  btn2ModalContainer: {
    flex: 1,
  },
  btn2: {
    borderRadius: 0,
    elevation: 0,
    borderBottomRightRadius: deviceWidth * 0.02,
    backgroundColor: Colors.COLOR_RED,
  },
  btn1: {
    borderRadius: 0,
    elevation: 0,
    borderBottomLeftRadius: deviceWidth * 0.02,
    backgroundColor: Colors.COLOR_GREEN,
  },
});

export default confirmModal;
