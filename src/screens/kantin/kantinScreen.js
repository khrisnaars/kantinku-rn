import React, {Component} from 'react';
import {
  Text,
  View,
  Container,
  Header,
  Body,
  Content,
  Right,
  Left,
  Icon,
  Toast,
  List,
  Input,
  ListItem,
  Button,
  CardItem,
} from 'native-base';
import {
  StyleSheet,
  Image,
  Dimensions,
  Platform,
  ImageBackground,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {CommonActions} from '@react-navigation/native';
import axios from 'react-native-axios';
import Spinner from 'react-native-loading-spinner-overlay';
import CurrencyFormat from 'react-currency-format';
import Modal from 'react-native-modal';

//DEVICE DIMENSION
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

//UTILS
import Logos from '../../utils/logos';
import Icons from '../../utils/icons';
import Colors from '../../utils/colors';
import GlobalVariables from '../../utils/globalVariables';
import {TouchableOpacity} from 'react-native-gesture-handler';

//RESET NAVIGATION
const resetLogin = CommonActions.reset({
  index: 1,
  routes: [{name: 'loginScreen'}],
});

//CUSTOM COMPONENT
import StatusBarPrimary from '../statusBar/statusBarPrimary';
import backgrounds from '../../utils/backgrounds';

class kantinScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dataUser: [],
      dataTrans: [],
      actionTrans: true,
      loading: true,
      modalConfirm: false,
      dataTransaksi: [],
    };
  }

  componentWillMount() {
    this.getData();
  }

  async getData() {
    var dataUser = await AsyncStorage.getItem('dataUser');

    this.setState(
      {
        dataUser: JSON.parse(dataUser),
      },
      () => this._getDataTransaksi(),
    );
  }

  async getDataTransaksi() {
    let url =
      GlobalVariables.URL_KANTINKU + GlobalVariables.URL_POST_DATA_TRANSAKSI;

    let parameter = {
      kios_id: this.state.dataUser.ID,
    };

    const res = await axios.post(url, parameter);

    return await res;
  }

  _getDataTransaksi() {
    this.getDataTransaksi()
      .then(response => {
        response = response.data;

        if (response.status == 1) {
          this.setState({loading: false, dataTransaksi: response.data});
        } else {
          this.setState({loading: false}, () => {
            Toast.show({
              text: response.error,
              buttonText: 'X',
              type: 'danger',
              duration: 3000,
            });
          });
        }
      })
      .catch(function(error) {
        this.setState({loading: false}, () => {
          Toast.show({
            text:
              'Terjadi kesalahan pastikan koneksi anda baik, dan hubungi administrator',
            buttonText: 'X',
            type: 'danger',
            duration: 3000,
          });
        });
      });
  }

  modalTerima(item) {
    this.setState({
      modalConfirm: true,
      dataTrans: item,
      actionTrans: true,
    });
  }

  modalTolak(item) {
    this.setState({
      modalConfirm: true,
      dataTrans: item,
      actionTrans: false,
    });
  }

  async Terima() {
    let url = GlobalVariables.URL_KANTINKU + GlobalVariables.URL_POST_TERIMA;

    let parameter = {
      id: this.state.dataTrans.ID,
      username: this.state.dataUser.USERNAME,
    };

    const res = await axios.post(url, parameter);

    return await res;
  }

  async Tolak() {
    let url = GlobalVariables.URL_KANTINKU + GlobalVariables.URL_POST_TOLAK;

    let parameter = {
      id: this.state.dataTrans.ID,
      username: this.state.dataUser.USERNAME,
      nama_kios: this.state.dataUser.NAMA,
      nim: this.state.dataTrans.NIM,
    };
    console.log(url);
    console.log(parameter);
    const res = await axios.post(url, parameter);

    return await res;
  }

  _Tolak() {
    this.setState(
      {
        loading: true,
      },
      () => {
        this.Tolak()
          .then(response => {
            response = response.data;

            if (response.status == 1) {
              this.setState(
                {
                  modalConfirm: false,
                },
                () => {
                  this._getDataTransaksi();
                  Toast.show({
                    text: 'Transaksi berhasil ditolak',
                    buttonText: 'X',
                    type: 'success',
                    duration: 3000,
                  });
                },
              );
            } else {
              this.setState({loading: false}, () => {
                Toast.show({
                  text: 'Gagal menolak transaksi',
                  buttonText: 'X',
                  type: 'danger',
                  duration: 3000,
                });
              });
            }
          })
          .catch(function(error) {
            this.setState({loading: false}, () => {
              Toast.show({
                text:
                  'Terjadi kesalahan pastikan koneksi anda baik, dan hubungi administrator',
                buttonText: 'X',
                type: 'danger',
                duration: 3000,
              });
            });
          });
      },
    );
  }

  _Terima() {
    this.setState(
      {
        loading: true,
      },
      () => {
        this.Terima()
          .then(response => {
            response = response.data;

            if (response.status == 1) {
              this.setState(
                {
                  modalConfirm: false,
                },
                () => {
                  this._getDataTransaksi();
                  Toast.show({
                    text: 'Transaksi berhasil diterima',
                    buttonText: 'X',
                    type: 'success',
                    duration: 3000,
                  });
                },
              );
            } else {
              this.setState({loading: false}, () => {
                Toast.show({
                  text: response.error,
                  buttonText: 'X',
                  type: 'danger',
                  duration: 3000,
                });
              });
            }
          })
          .catch(function(error) {
            this.setState({loading: false}, () => {
              Toast.show({
                text:
                  'Terjadi kesalahan pastikan koneksi anda baik, dan hubungi administrator',
                buttonText: 'X',
                type: 'danger',
                duration: 3000,
              });
            });
          });
      },
    );
  }

  _getHistory() {
    this.props.navigation.navigate('historyScreen', {section: 1});
  }

  _renderModalConfirm() {
    var action = 'menerima';
    if (!this.state.actionTrans) {
      action = 'menolak';
    }

    return (
      <Modal isVisible={this.state.modalConfirm}>
        <View
          style={{
            borderTopEndRadius: deviceWidth * 0.01,
            borderTopStartRadius: deviceWidth * 0.01,
            alignSelf: 'center',
            width: deviceWidth * 0.8,
            height: deviceHeight * 0.2,
            backgroundColor: Colors.COLOR_WHITE,
          }}>
          <Text style={{margin: deviceWidth * 0.03}}>
            Anda ingin {action} transaksi dengan kode {this.state.dataTrans.ID}{' '}
            dari {this.state.dataTrans.NIM} sejumlah{' '}
            {this.state.dataTrans.GRAND_TOTAL} ?
          </Text>
          <View style={{flexDirection: 'row', flex: 1}}>
            <View style={{flex: 1, alignSelf: 'flex-end'}}>
              <Button
                style={{
                  justifyContent: 'center',
                  backgroundColor: Colors.COLOR_RED,
                  borderBottomEndRadius: 0,
                  borderBottomstartRadius: 0,
                }}
                onPress={() => this.setState({modalConfirm: false})}>
                <Text>Batal</Text>
              </Button>
            </View>
            <View style={{flex: 1, alignSelf: 'flex-end'}}>
              <Button
                style={{
                  justifyContent: 'center',
                  backgroundColor: Colors.COLOR_GREEN,
                  borderBottomEndRadius: 0,
                  borderBottomstartRadius: 0,
                }}
                onPress={() =>
                  this.state.actionTrans ? this._Terima() : this._Tolak()
                }>
                <Text>Yakin</Text>
              </Button>
            </View>
          </View>
        </View>
      </Modal>
    );
  }

  logout() {
    this.setState(
      {
        loading: true,
      },
      () => {
        if (this.removeData('dataUser')) {
          this.setState({loading: false}, () => {
            this.props.navigation.dispatch(resetLogin);
          });
        } else {
          this.setState({loading: false}, () => {
            Toast.show({
              text: 'Gagal delete session anda',
              buttonText: 'X',
              type: 'danger',
              duration: 3000,
            });
          });
        }
      },
    );
  }

  async removeData(data) {
    try {
      await AsyncStorage.removeItem(data);
      return true;
    } catch (exception) {
      return false;
    }
  }

  _getMenu() {
    this.props.navigation.navigate('menuScreen');
  }

  renderRow(item) {
    return (
      <View style={Styles.mainItem}>
        <View style={{flex: 1}}>
          <View style={Styles.textIcon}>
            <Icon
              type="SimpleLineIcons"
              name="arrow-right"
              style={Styles.iconForm}
            />
            <View style={Styles.verticalLine} />
            <Text style={{fontFamily: 'segoeui', marginStart: 5}}>
              {item.ID}
            </Text>
          </View>
          <View style={Styles.textIcon}>
            <Icon type="SimpleLineIcons" name="user" style={Styles.iconForm} />
            <View style={Styles.verticalLine} />
            <Text style={{fontFamily: 'segoeui', marginStart: 5}}>
              {item.pemesan.NAMA} - {item.NIM}
            </Text>
          </View>
          <View style={Styles.textIcon}>
            <Icon type="SimpleLineIcons" name="clock" style={Styles.iconForm} />
            <View style={Styles.verticalLine} />
            <Text style={{fontFamily: 'segoeui', marginStart: 5}}>
              {item.CREATED_DATE} -{' '}
              {item.STATUS == 1
                ? 'Verified'
                : item.STATUS == 2
                ? 'Success'
                : 'Rejected'}
            </Text>
          </View>
          <View style={Styles.textIcon}>
            <Icon
              type="SimpleLineIcons"
              name="wallet"
              style={Styles.iconForm}
            />
            <View style={Styles.verticalLine} />
            <CurrencyFormat
              value={item.GRAND_TOTAL}
              displayType={'text'}
              thousandSeparator={true}
              renderText={value => (
                <Text style={{fontFamily: 'segoeui', marginStart: 5}}>
                  {item.JUMLAH_TOTAL + ' - ' + value}
                </Text>
              )}
            />
          </View>
          <Text style={{marginHorizontal: 10, fontFamily: 'segoeui'}}>
            Keterangan : {item.KETERANGAN}
          </Text>

          {item.transaksi_d.map(item => {
            return (
              <View
                style={{
                  backgroundColor: Colors.COLOR_BLACK_97,
                  padding: deviceWidth * 0.01,
                  margin: deviceWidth * 0.01,
                  flex: 1,
                }}>
                <Text style={{fontFamily: 'segoeui', fontSize: 20}}>
                  {item.NAMA}
                </Text>

                <View style={{display: 'flex', flexDirection: 'row'}}>
                  <View style={{display: 'flex', flex: 2}}>
                    <Text style={{fontFamily: 'segoeui', fontSize: 12}}>
                      {item.JNS_MENU}
                    </Text>
                    <Text style={{fontFamily: 'segoeui'}}>{item.HARGA}</Text>
                  </View>
                  <View
                    style={{
                      display: 'flex',
                      flex: 2,
                      backgroundColor: Colors.COLOR_PRIMARY,
                      justifyContent: 'center',
                      paddingHorizontal: deviceWidth * 0.01,
                    }}>
                    <CurrencyFormat
                      value={parseInt(item.JUMLAH) * parseInt(item.HARGA)}
                      displayType={'text'}
                      thousandSeparator={true}
                      renderText={value => (
                        <Text
                          style={{
                            fontSize: 20,
                            fontFamily: 'segoeui',
                            color: Colors.COLOR_WHITE,
                          }}>
                          {item.JUMLAH + ' | ' + value}
                        </Text>
                      )}
                    />
                  </View>
                </View>
              </View>
            );
          })}
        </View>

        <View
          style={{flex: 1, flexDirection: 'row', margin: deviceWidth * 0.03}}>
          <View style={{flex: 1}}>
            <Button
              style={{
                justifyContent: 'center',
                backgroundColor: Colors.COLOR_RED,
                borderBottomEndRadius: 0,
                borderBottomstartRadius: 0,
              }}
              onPress={() => this.modalTolak(item)}>
              <Text>Tolak</Text>
            </Button>
          </View>
          <View style={{flex: 1}}>
            <Button
              style={{
                justifyContent: 'center',
                backgroundColor: Colors.COLOR_GREEN,
                borderBottomEndRadius: 0,
                borderBottomstartRadius: 0,
              }}
              onPress={() => this.modalTerima(item)}>
              <Text>Terima</Text>
            </Button>
          </View>
        </View>
      </View>
    );
  }

  render() {
    return (
      <Container>
        <Spinner
          visible={this.state.loading}
          textContent={'Loading...'}
          textStyle={{color: Colors.COLOR_BLACK}}
        />

        {this._renderModalConfirm()}

        <Header style={{backgroundColor: Colors.COLOR_PRIMARY}}>
          <StatusBarPrimary />
          <View style={Styles.headerContainer}>
            <Text style={Styles.logoText}>Kantinku Mobile</Text>
          </View>
          <View style={{justifyContent: 'center'}}>
            <TouchableOpacity onPress={() => this.logout()}>
              <Icon
                type="SimpleLineIcons"
                name="share-alt"
                style={{alignSelf: 'center', color: Colors.COLOR_WHITE}}
              />
            </TouchableOpacity>
          </View>
        </Header>

        <ImageBackground
          source={backgrounds.background_kantinku_02}
          style={{width: deviceWidth, flex: 1}}>
          <View style={Styles.mainHeader}>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                <View style={Styles.textIcon}>
                  <Icon
                    type="SimpleLineIcons"
                    name="user"
                    style={Styles.iconForm}
                  />
                  <View style={Styles.verticalLine} />
                  <Text style={{fontFamily: 'segoeui', marginStart: 5}}>
                    {this.state.dataUser.NAMA}
                  </Text>
                </View>
                <View style={Styles.textIcon}>
                  <Icon
                    type="SimpleLineIcons"
                    name="wallet"
                    style={Styles.iconForm}
                  />
                  <View style={Styles.verticalLine} />
                  <Text style={{fontFamily: 'segoeui', marginStart: 5}}>
                    {this.state.dataUser.BANK +
                      ' - ' +
                      this.state.dataUser.REKENING}
                  </Text>
                </View>
              </View>
              <View style={{marginHorizontal: deviceWidth * 0.02}}>
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignSelf: 'center',
                  }}>
                  <TouchableOpacity onPress={() => this._getMenu()}>
                    <Icon
                      type="MaterialIcons"
                      name="restaurant-menu"
                      style={{alignSelf: 'center'}}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <View style={{marginHorizontal: deviceWidth * 0.02}}>
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignSelf: 'center',
                  }}>
                  <TouchableOpacity onPress={() => this._getHistory()}>
                    <Icon
                      type="MaterialCommunityIcons"
                      name="history"
                      style={{alignSelf: 'center'}}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>

          <List
            keyExtractor={item => item.ID}
            style={Styles.listContainer}
            dataArray={this.state.dataTransaksi}
            renderRow={item => this.renderRow(item)}
          />
        </ImageBackground>
      </Container>
    );
  }
}

const Styles = StyleSheet.create({
  headerContainer: {
    flexDirection: 'row',
    flex: 1,
    alignSelf: 'center',
  },
  logoText: {
    fontFamily: 'segoeui',
    fontWeight: 'bold',
    color: Colors.COLOR_WHITE,
  },
  mainHeader: {
    flexDirection: 'column',
    marginStart: deviceWidth * 0.02,
    marginEnd: deviceWidth * 0.02,
    marginTop: deviceWidth * 0.02,
    padding: deviceWidth * 0.02,
    borderRadius: deviceWidth * 0.01,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.9,
    shadowRadius: deviceWidth * 0.01,
    elevation: 1,
    height: deviceHeight * 0.09,
    backgroundColor: Colors.COLOR_WHITE,
  },
  mainItem: {
    flexDirection: 'column',
    marginStart: deviceWidth * 0.02,
    marginEnd: deviceWidth * 0.02,
    marginTop: deviceWidth * 0.02,
    padding: deviceWidth * 0.02,
    borderRadius: deviceWidth * 0.01,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.9,
    shadowRadius: deviceWidth * 0.01,
    elevation: 1,
    flex: 1,
    backgroundColor: Colors.COLOR_WHITE,
  },
  iconForm: {
    fontSize: 18,
    color: Colors.COLOR_BLACK_47,
    marginHorizontal: 10,
  },
  textIcon: {
    flexDirection: 'row',
  },
  verticalLine: {
    borderLeftWidth: 5,
    borderLeftColor: Colors.COLOR_PRIMARY,
  },
  listContainer: {
    flex: 1,
    margin: deviceWidth * 0.02,
  },
});

export default kantinScreen;
