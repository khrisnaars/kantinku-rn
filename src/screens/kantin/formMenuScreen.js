import React, {Component} from 'react';
import {
  Text,
  View,
  Container,
  Header,
  Body,
  Content,
  Right,
  Left,
  Icon,
  Toast,
  List,
  Card,
  ListItem,
  Button,
  CardItem,
  Input,
} from 'native-base';
import {
  StyleSheet,
  Image,
  Dimensions,
  Platform,
  ImageBackground,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {CommonActions} from '@react-navigation/native';
import axios from 'react-native-axios';
import Spinner from 'react-native-loading-spinner-overlay';
import CurrencyFormat from 'react-currency-format';
import moment from 'moment';

//DEVICE DIMENSION
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

//UTILS
import Logos from '../../utils/logos';
import Icons from '../../utils/icons';
import Colors from '../../utils/colors';
import GlobalVariables from '../../utils/globalVariables';
import {
  TouchableOpacity,
  TextInput,
  ScrollView,
} from 'react-native-gesture-handler';

//RESET NAVIGATION
const resetLogin = CommonActions.reset({
  index: 1,
  routes: [{name: 'loginScreen'}],
});

//CUSTOM COMPONENT
import StatusBarPrimary from '../statusBar/statusBarPrimary';
import backgrounds from '../../utils/backgrounds';
import {block} from 'react-native-reanimated';
import {Picker} from '@react-native-community/picker';

class formMenuScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      idMenu:
        this.props.route.params.section == 1
          ? this.props.route.params.dataMenu.ID
          : 'Buat Menu Baru',
      namaMenu:
        this.props.route.params.section == 1
          ? this.props.route.params.dataMenu.NAMA
          : '',
      hargaMenu:
        this.props.route.params.section == 1
          ? JSON.stringify(this.props.route.params.dataMenu.HARGA)
          : 0,
      dataUser: [],
      jenisMenu:
        this.props.route.params.section == 1
          ? this.props.route.params.dataMenu.JNS_MENU
          : 'MAKANAN',
      section: this.props.route.params.section,
    };
  }

  componentWillMount() {
    this.getData();
  }

  async getData() {
    var dataUser = await AsyncStorage.getItem('dataUser');

    this.setState({
      dataUser: JSON.parse(dataUser),
    });
  }

  async editMenu() {
    let url = GlobalVariables.URL_KANTINKU + GlobalVariables.URL_POST_EDIT_MENU;

    let parameter = {
      kios_nama: this.state.dataUser.USERNAME,
      menu_id: this.state.idMenu,
      nama: this.state.namaMenu,
      jns_menu: this.state.jenisMenu,
      harga: this.state.hargaMenu,
    };

    const res = await axios.post(url, parameter);

    return await res;
  }

  async simpanMenu() {
    let url =
      GlobalVariables.URL_KANTINKU + GlobalVariables.URL_POST_STORE_MENU;

    let parameter = {
      kios_nama: this.state.dataUser.USERNAME,
      kios_id: this.state.dataUser.ID,
      nama: this.state.namaMenu,
      jns_menu: this.state.jenisMenu,
      harga: this.state.hargaMenu,
    };

    const res = await axios.post(url, parameter);

    return await res;
  }

  _simpanMenu() {
    if (
      this.state.namaMenu == null ||
      this.state.namaMenu == '' ||
      this.state.jenisMenu == null ||
      this.state.jenisMenu == '' ||
      this.state.hargaMenu == null ||
      this.state.hargaMenu == ''
    ) {
      Toast.show({
        text: 'Pastikan anda mengisi semua data dengan benar...',
        buttonText: 'X',
        type: 'danger',
        duration: 3000,
      });
    } else {
      if (!/^\d+$/.test(this.state.hargaMenu)) {
        Toast.show({
          text:
            'Kalo isi harga hanya boleh angka ya, nanti konsumen kamu bingung !',
          buttonText: 'X',
          type: 'danger',
          duration: 3000,
        });
      } else {
        this.setState({loading: true}, () => {
          this.simpanMenu()
            .then(response => {
              response = response.data;
              if (response.status == 1) {
                this.setState({loading: false}, () => {
                  this.props.route.params.onGoBack();
                  this.props.navigation.goBack();
                  Toast.show({
                    text: response.pesan,
                    buttonText: 'X',
                    type: 'success',
                    duration: 3000,
                  });
                });
              } else {
                this.setState({loading: false}, () => {
                  Toast.show({
                    text: response.error,
                    buttonText: 'X',
                    type: 'danger',
                    duration: 3000,
                  });
                });
              }
            })
            .catch(function(error) {
              this.setState({loading: false}, () => {
                Toast.show({
                  text:
                    'Terjadi kesalahan pastikan koneksi anda baik, dan hubungi administrator',
                  buttonText: 'X',
                  type: 'danger',
                  duration: 3000,
                });
              });
            });
        });
      }
    }
  }
  _editMenu() {
    if (
      this.state.namaMenu == null ||
      this.state.namaMenu == '' ||
      this.state.jenisMenu == null ||
      this.state.jenisMenu == '' ||
      this.state.hargaMenu == null ||
      this.state.hargaMenu == ''
    ) {
      Toast.show({
        text: 'Pastikan anda mengisi semua data dengan benar...',
        buttonText: 'X',
        type: 'danger',
        duration: 3000,
      });
    } else {
      if (!/^\d+$/.test(this.state.hargaMenu)) {
        Toast.show({
          text:
            'Kalo isi harga hanya boleh angka ya, nanti konsumen kamu bingung !',
          buttonText: 'X',
          type: 'danger',
          duration: 3000,
        });
      } else {
        this.setState({loading: true}, () => {
          this.editMenu()
            .then(response => {
              response = response.data;
              if (response.status == 1) {
                this.setState({loading: false}, () => {
                  this.props.route.params.onGoBack();
                  this.props.navigation.goBack();
                  Toast.show({
                    text: response.pesan,
                    buttonText: 'X',
                    type: 'success',
                    duration: 3000,
                  });
                });
              } else {
                this.setState({loading: false}, () => {
                  Toast.show({
                    text: response.error,
                    buttonText: 'X',
                    type: 'danger',
                    duration: 3000,
                  });
                });
              }
            })
            .catch(function(error) {
              this.setState({loading: false}, () => {
                Toast.show({
                  text:
                    'Terjadi kesalahan pastikan koneksi anda baik, dan hubungi administrator',
                  buttonText: 'X',
                  type: 'danger',
                  duration: 3000,
                });
              });
            });
        });
      }
    }
  }

  async hapusMenu() {
    let url =
      GlobalVariables.URL_KANTINKU + GlobalVariables.URL_POST_HAPUS_MENU;

    let parameter = {
      menu_id: this.state.idMenu,
    };
    const res = await axios.post(url, parameter);

    return await res;
  }
  _hapusMenu() {
    this.hapusMenu()
      .then(response => {
        response = response.data;
        if (response.status == 1) {
          this.setState({loading: false}, () => {
            this.props.route.params.onGoBack();
            this.props.navigation.goBack();
            Toast.show({
              text: response.pesan,
              buttonText: 'X',
              type: 'success',
              duration: 3000,
            });
          });
        } else {
          this.setState({loading: false}, () => {
            Toast.show({
              text: response.error,
              buttonText: 'X',
              type: 'danger',
              duration: 3000,
            });
          });
        }
      })
      .catch(function(error) {
        this.setState({loading: false}, () => {
          Toast.show({
            text:
              'Terjadi kesalahan pastikan koneksi anda baik, dan hubungi administrator',
            buttonText: 'X',
            type: 'danger',
            duration: 3000,
          });
        });
      });
  }

  render() {
    return (
      <Container>
        <Spinner
          visible={this.state.loading}
          textContent={'Loading...'}
          textStyle={{color: Colors.COLOR_BLACK}}
        />

        <ImageBackground
          source={backgrounds.background_kantinku_02}
          style={{width: deviceWidth, flex: 1}}>
          <Card
            style={{
              width: deviceWidth * 0.95,
              borderRadius: 20,
              marginTop: deviceWidth * 0.05,
              alignSelf: 'center',
              display: 'flex',
              backgroundColor: Colors.COLOR_BLACK_97,
            }}>
            <CardItem
              style={{
                height: deviceHeight * 0.1,
                flexDirection: 'column',
                borderTopEndRadius: 20,
                borderTopStartRadius: 20,
                backgroundColor: Colors.COLOR_PRIMARY,
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontFamily: 'segoeui',
                  fontSize: 20,
                  color: Colors.COLOR_WHITE,
                }}>
                {this.state.idMenu}
              </Text>
            </CardItem>
            <CardItem>
              <Text>Nama Makanan : </Text>
              <Input
                style={{
                  marginHorizontal: deviceWidth * 0.03,
                  borderColor: Colors.COLOR_PRIMARY_60,
                  borderWidth: 2,
                }}
                onChangeText={text => this.setState({namaMenu: text})}
                value={this.state.namaMenu}
              />
            </CardItem>
            <View
              style={{
                padding: deviceWidth * 0.04,
                backgroundColor: Colors.COLOR_WHITE,
              }}>
              <Text>Jenis Makanan : </Text>
              <Picker
                selectedValue={this.state.jenisMenu}
                onValueChange={itemValue =>
                  this.setState({jenisMenu: itemValue})
                }>
                <Picker.Item label="MAKANAN" value="MAKANAN" />
                <Picker.Item label="MINUMAN" value="MINUMAN" />
                <Picker.Item label="MAKANAN+MINUMAN" value="MAKANAN+MINUMAN" />
              </Picker>
            </View>
            <CardItem>
              <Text>Harga : </Text>
              <Input
                style={{
                  marginHorizontal: deviceWidth * 0.03,
                  borderColor: Colors.COLOR_PRIMARY_60,
                  borderWidth: 2,
                }}
                keyboardType={'number-pad'}
                onChangeText={text => this.setState({hargaMenu: text})}
                value={this.state.hargaMenu}
              />
            </CardItem>
            <CardItem>
              <Button
                onPress={() =>
                  this.props.route.params.section == 1
                    ? this._editMenu()
                    : this._simpanMenu()
                }
                style={{
                  display: 'flex',
                  flex: 1,
                  backgroundColor: Colors.COLOR_GREEN,
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    color: Colors.COLOR_WHITE,
                    textAlign: 'center',
                    fontFamily: 'segoeui',
                    fontWeight: 'bold',
                    margin: deviceWidth * 0.02,
                  }}>
                  SIMPAN
                </Text>
              </Button>
            </CardItem>
            {this.state.section == 1 ? (
              <CardItem>
                <Button
                  onPress={() => this._hapusMenu()}
                  style={{
                    display: 'flex',
                    flex: 1,
                    backgroundColor: Colors.COLOR_RED,
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      color: Colors.COLOR_WHITE,
                      textAlign: 'center',
                      fontFamily: 'segoeui',
                      fontWeight: 'bold',
                      margin: deviceWidth * 0.02,
                    }}>
                    HAPUS
                  </Text>
                </Button>
              </CardItem>
            ) : null}
          </Card>
        </ImageBackground>
      </Container>
    );
  }
}

const Styles = StyleSheet.create({
  headerContainer: {
    flexDirection: 'row',
    flex: 1,
    alignSelf: 'center',
  },
  logoText: {
    fontFamily: 'segoeui',
    fontWeight: 'bold',
    color: Colors.COLOR_WHITE,
  },
  mainHeader: {
    flexDirection: 'row',
    marginTop: deviceWidth * 0.02,
    padding: deviceWidth * 0.02,
    borderRadius: deviceWidth * 0.01,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.9,
    shadowRadius: deviceWidth * 0.01,
    elevation: 1,
    zIndex: 2,
    backgroundColor: Colors.COLOR_WHITE,
  },
  iconForm: {
    fontSize: 18,
    color: Colors.COLOR_BLACK_47,
    marginHorizontal: 10,
  },
  textIcon: {
    flexDirection: 'row',
  },
  verticalLine: {
    borderLeftWidth: 5,
    borderLeftColor: Colors.COLOR_PRIMARY,
  },
  listContainer: {
    flex: 1,
    margin: deviceWidth * 0.02,
  },
});

export default formMenuScreen;
