import React, {Component} from 'react';
import {
  Text,
  View,
  Container,
  Header,
  Body,
  Content,
  Right,
  Left,
  Icon,
  Toast,
  List,
  Input,
  ListItem,
  Button,
  Col,
} from 'native-base';
import {
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  Platform,
  ImageBackground,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {CommonActions} from '@react-navigation/native';
import axios from 'react-native-axios';
import Spinner from 'react-native-loading-spinner-overlay';
import CurrencyFormat from 'react-currency-format';
import moment from 'moment';
import Modal from 'react-native-modal';
import DatePicker from 'react-native-datepicker';
import ToggleSwitch from 'toggle-switch-react-native';
import {Picker} from '@react-native-community/picker';

//DEVICE DIMENSION
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

//UTILS
import Logos from '../../utils/logos';
import Icons from '../../utils/icons';
import Colors from '../../utils/colors';
import GlobalVariables from '../../utils/globalVariables';

//RESET NAVIGATION
const resetLogin = CommonActions.reset({
  index: 1,
  routes: [{name: 'loginScreen'}],
});

//CUSTOM COMPONENT
import StatusBarPrimary from '../statusBar/statusBarPrimary';
import backgrounds from '../../utils/backgrounds';

class menuScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      dataMenu: [],
    };
  }

  componentWillMount() {
    this.getData();
  }

  async getData() {
    var dataUser = await AsyncStorage.getItem('dataUser');

    this.setState(
      {
        dataUser: JSON.parse(dataUser),
      },
      () => this._getdataMenu(this.state.dataUser),
    );
  }

  async getdataMenu(data) {
    let url = GlobalVariables.URL_KANTINKU + GlobalVariables.URL_POST_MENU;

    let parameter = {
      kios_id: data.ID,
    };

    const res = await axios.post(url, parameter);

    return await res;
  }

  _getdataMenu(data) {
    this.getdataMenu(data)
      .then(response => {
        response = response.data;
        if (response.status == 1) {
          this.setState({loading: false, dataMenu: response.pesan});
        } else {
          this.setState({loading: false}, () => {
            Toast.show({
              text: response.error,
              buttonText: 'X',
              type: 'danger',
              duration: 3000,
            });
          });
        }
      })
      .catch(function(error) {
        this.setState({loading: false}, () => {
          Toast.show({
            text:
              'Terjadi kesalahan pastikan koneksi anda baik, dan hubungi administrator',
            buttonText: 'X',
            type: 'danger',
            duration: 3000,
          });
        });
      });
  }

  _editMenu(dataMenu) {
    this.props.navigation.navigate('formMenuScreen', {
      dataMenu: dataMenu,
      section: 1,
      onGoBack: () => this._getdataMenu(this.state.dataUser),
    });
  }

  _createMenu() {
    this.props.navigation.navigate('formMenuScreen', {
      section: 0,
      onGoBack: () => this._getdataMenu(this.state.dataUser),
    });
  }

  async gantiStatus(item) {
    let url =
      GlobalVariables.URL_KANTINKU +
      GlobalVariables.URL_POST_UPDATE_STATUS_MENU;

    let parameter = {
      kios_nama: this.state.dataUser.USERNAME,
      menu_id: item.ID,
      status: item.STATUS == 1 ? 0 : 1,
    };

    const res = await axios.post(url, parameter);

    return await res;
  }

  _gantiStatus(item) {
    this.gantiStatus(item)
      .then(response => {
        response = response.data;
        if (response.status == 1) {
          this.setState({loading: false}, () => {
            this._getdataMenu(this.state.dataUser);
            Toast.show({
              text: response.pesan,
              buttonText: 'X',
              type: 'success',
              duration: 3000,
            });
          });
        } else {
          this.setState({loading: false}, () => {
            Toast.show({
              text: response.error,
              buttonText: 'X',
              type: 'danger',
              duration: 3000,
            });
          });
        }
      })
      .catch(function(error) {
        this.setState({loading: false}, () => {
          Toast.show({
            text:
              'Terjadi kesalahan pastikan koneksi anda baik, dan hubungi administrator',
            buttonText: 'X',
            type: 'danger',
            duration: 3000,
          });
        });
      });
  }

  renderRow(item) {
    return (
      <View style={Styles.mainHeader}>
        <View
          style={{
            backgroundColor: Colors.COLOR_WHITE,
            margin: deviceWidth * 0.02,
            padding: deviceWidth * 0.01,
            flex: 1,
          }}>
          <Text style={{fontFamily: 'segoeui', fontSize: 20}}>{item.NAMA}</Text>
          <View style={{display: 'flex', flexDirection: 'row'}}>
            <View style={{display: 'flex', flex: 2}}>
              <Text style={{fontFamily: 'segoeui', fontSize: 18}}>
                {item.JNS_MENU}
              </Text>
              <CurrencyFormat
                value={item.HARGA}
                displayType={'text'}
                thousandSeparator={true}
                renderText={value => (
                  <Text style={{fontFamily: 'segoeui', fontSize: 18}}>
                    {value}
                  </Text>
                )}
              />
            </View>
            <View
              style={{
                display: 'flex',
                flex: 1,
                backgroundColor: Colors.COLOR_PRIMARY,
              }}>
              <TouchableOpacity
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'center',
                }}
                onPress={() => this._editMenu(item)}>
                <Text
                  style={{
                    fontFamily: 'segoeui',
                    alignSelf: 'center',
                    color: Colors.COLOR_WHITE,
                  }}>
                  Ubah Menu
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{marginTop: deviceWidth * 0.03, alignSelf: 'flex-end'}}>
            <ToggleSwitch
              isOn={item.STATUS == 1 ? true : false}
              onColor="green"
              offColor="red"
              label="Menu Tersedia : "
              size="medium"
              onToggle={isOn =>
                this.setState({loading: true}, () => this._gantiStatus(item))
              }
            />
          </View>
        </View>
      </View>
    );
  }

  render() {
    return (
      <Container>
        <Spinner
          visible={this.state.loading}
          textContent={'Loading...'}
          textStyle={{color: Colors.COLOR_BLACK}}
        />
        <ImageBackground
          source={backgrounds.background_kantinku_02}
          style={{width: deviceWidth, flex: 1}}>
          <List
            keyExtractor={item => item.ID}
            style={Styles.listContainer}
            dataArray={this.state.dataMenu}
            renderRow={item => this.renderRow(item)}
          />
          <TouchableOpacity
            style={{
              justifyContent: 'center',
              backgroundColor: Colors.COLOR_WHITE,
            }}
            onPress={() => this._createMenu()}>
            <Text
              style={{
                fontFamily: 'segoeui',
                alignSelf: 'center',
                marginVertical: deviceWidth * 0.05,
              }}>
              Buat Menu Baru
            </Text>
          </TouchableOpacity>
        </ImageBackground>
      </Container>
    );
  }
}

const Styles = StyleSheet.create({
  headerContainer: {
    flexDirection: 'row',
    flex: 1,
    alignSelf: 'center',
  },
  logoText: {
    fontFamily: 'segoeui',
    fontWeight: 'bold',
    color: Colors.COLOR_WHITE,
  },
  mainHeader: {
    flexDirection: 'row',
    marginTop: deviceWidth * 0.02,
    padding: deviceWidth * 0.02,
    borderRadius: deviceWidth * 0.01,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.9,
    shadowRadius: deviceWidth * 0.01,
    elevation: 1,
    zIndex: 2,
    backgroundColor: Colors.COLOR_WHITE,
  },
  iconForm: {
    fontSize: 18,
    color: Colors.COLOR_BLACK_47,
    marginHorizontal: 10,
  },
  textIcon: {
    flexDirection: 'row',
  },
  verticalLine: {
    borderLeftWidth: 5,
    borderLeftColor: Colors.COLOR_PRIMARY,
  },
  listContainer: {
    flex: 1,
    margin: deviceWidth * 0.02,
  },
});

export default menuScreen;
