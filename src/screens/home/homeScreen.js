import React, {Component} from 'react';
import {
  Text,
  View,
  Container,
  Header,
  Body,
  Content,
  Right,
  Left,
  Input,
  Icon,
  Toast,
  Button,
  ListItem,
  Row,
} from 'native-base';
import {
  StyleSheet,
  Image,
  Dimensions,
  Platform,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import QRCodeScanner from 'react-native-qrcode-scanner';
import {RNCamera} from 'react-native-camera';
import {CommonActions} from '@react-navigation/native';
import axios from 'react-native-axios';
import Spinner from 'react-native-loading-spinner-overlay';
import CurrencyFormat from 'react-currency-format';
import Modal from 'react-native-modal';

//DEVICE DIMENSION
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

//UTILS
import Logos from '../../utils/logos';
import Icons from '../../utils/icons';
import Colors from '../../utils/colors';
import GlobalVariables from '../../utils/globalVariables';
import {ScrollView, TextInput} from 'react-native-gesture-handler';

//CAMERA
let checkAndroidPermission = true;
if (Platform.OS === 'android' && Platform.Version < 23) {
  checkAndroidPermission = false;
}

//RESET NAVIGATION
const resetLogin = CommonActions.reset({
  index: 1,
  routes: [{name: 'loginScreen'}],
});

//CUSTOM COMPONENT
import StatusBarPrimary from '../statusBar/statusBarPrimary';
import backgrounds from '../../utils/backgrounds';

class homeScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dataUser: [],
      saldoContainer: true,
      loading: true,
      saldo: 0,
      kiosId: null,
      namaKios: null,
      keterangan: null,
      modalMenu: false,
      dataMenu: [],
      dataPesanan: [],
      modalConfirm: false,
      totalHarga: 0,
      totalJumlah: 0,
    };
  }

  componentWillMount() {
    this.getData();
  }

  async getData() {
    var dataUser = await AsyncStorage.getItem('dataUser');

    this.setState(
      {
        dataUser: JSON.parse(dataUser),
      },
      () => this._getSaldo(),
    );
  }

  async getsaldo() {
    let url = GlobalVariables.URL_KANTINKU + GlobalVariables.URL_POST_GETSALDO;

    let parameter = {
      nim: this.state.dataUser.NIM,
    };

    const res = await axios.post(url, parameter);

    return await res;
  }

  _getSaldo() {
    this.setState({loading: true}, () => {
      this.getsaldo()
        .then(response => {
          response = response.data;
          if (response.status == 1) {
            this.setState({
              saldo: response.pesan,
              loading: false,
            });
          } else {
            this.setState({loading: false}, () => {
              Toast.show({
                text: response.error,
                buttonText: 'X',
                type: 'danger',
                duration: 3000,
              });
            });
          }
        })
        .catch(function(error) {
          this.setState({loading: false}, () => {
            Toast.show({
              text:
                'Terjadi kesalahan pastikan koneksi anda baik, dan hubungi administrator',
              buttonText: 'X',
              type: 'danger',
              duration: 3000,
            });
          });
        });
    });
  }

  _renderCamera() {
    return (
      <QRCodeScanner
        onRead={this.onSuccess}
        flashMode={RNCamera.Constants.FlashMode.auto}
        cameraProps={{captureAudio: false}}
        checkAndroid6Permissions={checkAndroidPermission}
        ref={node => {
          this.scanner = node;
        }}
        showMarker={true}
      />
    );
  }

  logout() {
    this.setState(
      {
        loading: true,
      },
      () => {
        if (this.removeData('dataUser')) {
          this.setState({loading: false}, () => {
            this.props.navigation.dispatch(resetLogin);
          });
        } else {
          this.setState({loading: false}, () => {
            Toast.show({
              text: 'Gagal delete session anda',
              buttonText: 'X',
              type: 'danger',
              duration: 3000,
            });
          });
        }
      },
    );
  }

  async removeData(data) {
    try {
      await AsyncStorage.removeItem(data);
      return true;
    } catch (exception) {
      return false;
    }
  }

  _getMutasi() {
    this.props.navigation.navigate('mutasiScreen');
  }

  _getHistory() {
    this.props.navigation.navigate('historyScreen', {section: 0});
  }

  async getMenu() {
    let url = GlobalVariables.URL_KANTINKU + GlobalVariables.URL_POST_MENU;

    let parameter = {
      kios_id: this.state.kiosId,
    };

    const res = await axios.post(url, parameter);

    return await res;
  }

  _getMenu() {
    this.setState({loading: true}, () => {
      this.getMenu()
        .then(response => {
          response = response.data;

          if (response.status == 1) {
            this.setState({
              dataMenu: response.pesan != null ? response.pesan : [],
              loading: false,
            });
          } else {
            this.setState({loading: false}, () => {
              Toast.show({
                text: response.error,
                buttonText: 'X',
                type: 'danger',
                duration: 3000,
              });
            });
          }
        })
        .catch(function(error) {
          this.setState({loading: false}, () => {
            Toast.show({
              text:
                'Terjadi kesalahan pastikan koneksi anda baik, dan hubungi administrator',
              buttonText: 'X',
              type: 'danger',
              duration: 3000,
            });
          });
        });
    });
  }

  onSuccess = data => {
    var dataJSON = data.data;
    var dataQR = JSON.parse(dataJSON);

    if (
      dataQR.kios_id != undefined &&
      dataQR.kios_id != null &&
      dataQR.nama_kios != undefined &&
      dataQR.nama_kios != null
    ) {
      this.setState(
        {
          modalMenu: true,
          kiosId: dataQR.kios_id,
          namaKios: dataQR.nama_kios,
        },
        () => this._getMenu(),
      );
    } else {
      Toast.show({
        text: 'Gagal mengenali QR Code',
        buttonText: 'X',
        type: 'danger',
        duration: 3000,
      });
      this.scanner.reactivate();
    }
  };

  async getBayar() {
    let url = GlobalVariables.URL_KANTINKU + GlobalVariables.URL_POST_BAYAR;

    let parameter = {
      kios_id: this.state.kiosId,
      nama_kios: this.state.namaKios,
      nominal: this.state.totalHarga,
      nim: this.state.dataUser.NIM,
      jumlah: this.state.totalJumlah,
      keterangan: this.state.keterangan,
      pesanan: this.state.dataPesanan,
    };

    const res = await axios.post(url, parameter);

    return await res;
  }

  transBayar() {
    this.getBayar()
      .then(response => {
        response = response.data;
        if (response.status == 1) {
          this.setState(
            {modalConfirm: false, modalMenu: false, loading: false},
            () => {
              this._getHistory();
            },
          );
        } else {
          this.setState({loading: false, modalConfirm: false}, () => {
            Toast.show({
              text: response.error,
              buttonText: 'X',
              type: 'danger',
              duration: 3000,
            });
          });
        }
      })
      .catch(function(error) {
        this.setState({loading: false, modalConfirm: false}, () => {
          Toast.show({
            text:
              'Terjadi kesalahan pastikan koneksi anda baik, dan hubungi administrator',
            buttonText: 'X',
            type: 'danger',
            duration: 3000,
          });
        });
      });
  }

  _Bayar() {
    if (this.state.totalHarga != 0 && this.state.totalJumlah != 0) {
      if (this.state.saldo > this.state.totalHarga) {
        var dataPesan = [];
        this.state.dataMenu.map(item => {
          if (item.JUMLAH != 0) {
            dataPesan.push(item);
          }
        });

        this.setState({
          dataPesanan: dataPesan,
          loading: true,
          modalConfirm: true,
        });
      } else {
        Toast.show({
          text: 'Saldo anda tidak mencukupi',
          buttonText: 'X',
          type: 'danger',
          duration: 3000,
        });
        this.setState(
          {
            modalMenu: false,
          },
          () => {
            this.scanner.reactivate();
          },
        );
      }
    } else {
      Toast.show({
        text: 'Pesanan anda masi kosong :(',
        buttonText: 'X',
        type: 'danger',
        duration: 3000,
      });
    }
  }

  updateArray(item, action) {
    if (action == 1) {
      let dataMenu = [...this.state.dataMenu];
      let index = dataMenu.findIndex(el => el.ID === item.ID);

      dataMenu[index].JUMLAH = item.JUMLAH + 1;
      this.setState({dataMenu: dataMenu});

      this.setState({
        totalJumlah: this.state.totalJumlah + 1,
        totalHarga: this.state.totalHarga + item.HARGA,
      });
    } else {
      let dataMenu = [...this.state.dataMenu];
      let index = dataMenu.findIndex(el => el.ID === item.ID);

      dataMenu[index].JUMLAH =
        this.state.dataMenu[index].JUMLAH == 0 ? 0 : item.JUMLAH - 1;
      this.setState({dataMenu: dataMenu});

      this.setState({
        totalJumlah:
          this.state.totalJumlah - (this.state.totalJumlah == 0 ? 0 : 1),
        totalHarga:
          this.state.totalHarga - (this.state.totalHarga == 0 ? 0 : item.HARGA),
      });
    }
  }

  _renderModalMenu() {
    return (
      <Modal isVisible={this.state.modalMenu}>
        <View
          style={{
            borderRadius: deviceWidth * 0.01,
            alignSelf: 'center',
            width: deviceWidth * 0.9,
            height: deviceHeight * 0.8,
            backgroundColor: Colors.COLOR_PRIMARY_30,
          }}>
          <View
            style={{
              borderRadius: deviceWidth * 0.01,
              width: deviceWidth * 0.9,
              height: deviceHeight * 0.1,
              backgroundColor: Colors.COLOR_PRIMARY,
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'segoeui',
                textAlign: 'center',
                color: Colors.COLOR_WHITE,
                fontSize: 18,
              }}>
              Menu {this.state.namaKios}
            </Text>
          </View>

          {this.state.dataMenu.length > 0 ? (
            <ScrollView style={{flex: 1}}>
              {this.state.dataMenu.map((item, index) => {
                if (item.STATUS == 1) {
                  return (
                    <View
                      style={{
                        backgroundColor: Colors.COLOR_WHITE,
                        margin: deviceWidth * 0.02,
                        padding: deviceWidth * 0.01,
                      }}>
                      <Text style={{fontFamily: 'segoeui'}}>{item.NAMA}</Text>

                      <View style={{display: 'flex', flexDirection: 'row'}}>
                        <View style={{display: 'flex', flex: 2}}>
                          <Text style={{fontFamily: 'segoeui'}}>
                            {item.JNS_MENU}
                          </Text>
                          <Text style={{fontFamily: 'segoeui'}}>
                            {item.HARGA}
                          </Text>
                        </View>
                        <View
                          style={{
                            display: 'flex',
                            flex: 1,
                            backgroundColor: Colors.COLOR_PRIMARY,
                          }}>
                          <View style={{flex: 1, flexDirection: 'row'}}>
                            <View
                              style={{
                                flex: 1,
                                alignSelf: 'center',
                                justifyContent: 'center',
                              }}>
                              <TouchableOpacity
                                onPress={() => this.updateArray(item, 0)}>
                                <Icon
                                  style={{
                                    textAlign: 'center',
                                    color: Colors.COLOR_WHITE,
                                  }}
                                  type={'MaterialIcons'}
                                  name="remove-circle"
                                />
                              </TouchableOpacity>
                            </View>
                            <View
                              style={{
                                flex: 1,
                                alignSelf: 'center',
                                justifyContent: 'center',
                              }}>
                              <Text
                                style={{
                                  textAlign: 'center',
                                  fontWeight: 'bold',
                                  fontFamily: 'segoeui',
                                  color: Colors.COLOR_WHITE,
                                }}>
                                {item.JUMLAH}
                              </Text>
                            </View>
                            <View
                              style={{
                                flex: 1,
                                alignSelf: 'center',
                                justifyContent: 'center',
                              }}>
                              <TouchableOpacity
                                onPress={() => this.updateArray(item, 1)}>
                                <Icon
                                  style={{
                                    textAlign: 'center',
                                    color: Colors.COLOR_WHITE,
                                  }}
                                  type={'MaterialIcons'}
                                  name="add-circle"
                                />
                              </TouchableOpacity>
                            </View>
                          </View>
                        </View>
                      </View>
                    </View>
                  );
                } else {
                  return (
                    <View
                      style={{
                        backgroundColor: Colors.COLOR_BLACK_60,
                        margin: deviceWidth * 0.02,
                        padding: deviceWidth * 0.01,
                      }}>
                      <Text style={{fontFamily: 'segoeui'}}>{item.NAMA}</Text>

                      <View style={{display: 'flex', flexDirection: 'row'}}>
                        <View style={{display: 'flex', flex: 2}}>
                          <Text style={{fontFamily: 'segoeui'}}>
                            {item.JNS_MENU}
                          </Text>
                          <Text style={{fontFamily: 'segoeui'}}>
                            {item.HARGA}
                          </Text>
                        </View>
                        <View
                          style={{
                            display: 'flex',
                            flex: 1,
                            backgroundColor: Colors.COLOR_PRIMARY,
                          }}>
                          <View style={{flex: 1, flexDirection: 'row'}}>
                            <View
                              style={{
                                flex: 1,
                                alignSelf: 'center',
                                justifyContent: 'center',
                              }}>
                              <Text
                                style={{
                                  textAlign: 'center',
                                  fontWeight: 'bold',
                                  fontFamily: 'segoeui',
                                  color: Colors.COLOR_WHITE,
                                }}>
                                HABIS
                              </Text>
                            </View>
                          </View>
                        </View>
                      </View>
                    </View>
                  );
                }
              })}
            </ScrollView>
          ) : (
            <Text
              style={{
                textAlign: 'center',
                fontFamily: 'segoeui',
                fontWeight: 'bold',
                color: Colors.COLOR_WHITE,
                flex: 1,
                marginVertical: deviceWidth * 0.02,
              }}>
              Tidak Ada Menu
            </Text>
          )}
          <View
            style={{
              backgroundColor: Colors.COLOR_WHITE,
              height: deviceHeight * 0.1,
              justifyContent: 'center',
            }}>
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
                padding: deviceWidth * 0.02,
              }}>
              <View style={{display: 'flex'}}>
                <Text style={{fontFamily: 'segoeui', textAlign: 'right'}}>
                  Jumlah :{' '}
                </Text>
                <Text style={{fontFamily: 'segoeui', textAlign: 'right'}}>
                  Total :{' '}
                </Text>
              </View>
              <View
                style={{
                  display: 'flex',
                  flex: 1,
                  marginHorizontal: deviceWidth * 0.02,
                }}>
                <Text style={{fontFamily: 'segoeui', textAlign: 'right'}}>
                  {this.state.totalJumlah}
                </Text>
                <CurrencyFormat
                  value={this.state.totalHarga}
                  displayType={'text'}
                  thousandSeparator={true}
                  renderText={value => (
                    <Text style={{fontFamily: 'segoeui', textAlign: 'right'}}>
                      {value}
                    </Text>
                  )}
                />
              </View>
              <TouchableOpacity
                onPress={() =>
                  this.setState({modalMenu: false}, () =>
                    this.scanner.reactivate(),
                  )
                }
                style={{
                  display: 'flex',
                  flex: 1,
                  backgroundColor: Colors.COLOR_RED,
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    color: Colors.COLOR_WHITE,
                    textAlign: 'center',
                    fontFamily: 'segoeui',
                    fontWeight: 'bold',
                  }}>
                  KEMBALI
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this._Bayar()}
                style={{
                  display: 'flex',
                  flex: 1,
                  backgroundColor: Colors.COLOR_GREEN,
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    color: Colors.COLOR_WHITE,
                    textAlign: 'center',
                    fontFamily: 'segoeui',
                    fontWeight: 'bold',
                  }}>
                  BAYAR
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  }

  _renderModalConfirm() {
    return (
      <Modal isVisible={this.state.modalConfirm}>
        <View
          style={{
            borderTopEndRadius: deviceWidth * 0.01,
            borderTopStartRadius: deviceWidth * 0.01,
            alignSelf: 'center',
            width: deviceWidth * 0.8,
            height: deviceHeight * 0.3,
            backgroundColor: Colors.COLOR_WHITE,
          }}>
          <Text style={{margin: deviceWidth * 0.03}}>
            Anda ingin membayar pada {this.state.namaKios} sejumlah{' '}
            {this.state.totalHarga} ?
          </Text>
          <Text style={{marginHorizontal: deviceWidth * 0.03}}>
            Keterangan :{' '}
          </Text>
          <Input
            style={{
              marginHorizontal: deviceWidth * 0.03,
              borderColor: Colors.COLOR_PRIMARY_60,
              borderWidth: 2,
            }}
            onChangeText={text => this.setState({keterangan: text})}
            value={this.state.keterangan}
          />
          <View style={{flexDirection: 'row', flex: 1}}>
            <View style={{flex: 1, alignSelf: 'flex-end'}}>
              <Button
                style={{
                  justifyContent: 'center',
                  backgroundColor: Colors.COLOR_RED,
                  borderBottomEndRadius: 0,
                  borderBottomstartRadius: 0,
                }}
                onPress={() => this.setState({modalConfirm: false})}>
                <Text>Batal</Text>
              </Button>
            </View>
            <View style={{flex: 1, alignSelf: 'flex-end'}}>
              <Button
                style={{
                  justifyContent: 'center',
                  backgroundColor: Colors.COLOR_GREEN,
                  borderBottomEndRadius: 0,
                  borderBottomstartRadius: 0,
                }}
                onPress={() => this.transBayar()}>
                <Text>Bayar</Text>
              </Button>
            </View>
          </View>
        </View>
      </Modal>
    );
  }

  _renderSaldo() {
    return (
      <View style={Styles.secondHeader}>
        <View style={Styles.poinContainer}>
          <View style={Styles.curencyContainer}>
            <Text
              style={{
                fontFamily: 'segoeui',
                fontWeight: 'bold',
                color: Colors.COLOR_WHITE,
                fontSize: 15,
              }}>
              Poin
            </Text>
            <CurrencyFormat
              value={this.state.saldo}
              displayType={'text'}
              thousandSeparator={true}
              renderText={value => (
                <Text
                  style={{
                    fontSize: 20,
                    fontFamily: 'segoeui',
                    color: Colors.COLOR_WHITE,
                  }}>
                  {value}
                </Text>
              )}
            />
          </View>
          <View style={Styles.refreshContainer}>
            <TouchableOpacity onPress={() => this._getSaldo()}>
              <Icon
                type="SimpleLineIcons"
                name="reload"
                style={Styles.iconRefresh}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this._getMutasi()}>
              <Icon
                type="SimpleLineIcons"
                name="wallet"
                style={Styles.iconRefresh}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this._getHistory()}>
              <Icon
                type="MaterialCommunityIcons"
                name="history"
                style={{
                  fontSize: 35,
                  margin: deviceWidth * 0.02,
                  color: Colors.COLOR_WHITE,
                }}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
  render() {
    var arrow = 'arrow-up';

    if (this.state.saldoContainer) {
      arrow = 'arrow-down';
    }
    return (
      <Container>
        <Spinner
          visible={this.state.loading}
          textContent={'Loading...'}
          textStyle={{color: Colors.COLOR_BLACK}}
        />

        {this._renderModalMenu()}
        {this._renderModalConfirm()}

        <Header style={{backgroundColor: Colors.COLOR_PRIMARY}}>
          <StatusBarPrimary />
          <View style={Styles.headerContainer}>
            <Text style={Styles.logoText}>Kantinku Mobile</Text>
          </View>
          <View style={{justifyContent: 'center'}}>
            <TouchableOpacity onPress={() => this.logout()}>
              <Icon
                type="SimpleLineIcons"
                name="share-alt"
                style={{alignSelf: 'center', color: Colors.COLOR_WHITE}}
              />
            </TouchableOpacity>
          </View>
        </Header>

        <ImageBackground
          source={backgrounds.background_kantinku_02}
          style={{width: deviceWidth, flex: 1}}>
          <View style={Styles.mainContainer}>
            <View style={Styles.mainHeader}>
              <View style={{flex: 1}}>
                <View style={Styles.textIcon}>
                  <Icon
                    type="SimpleLineIcons"
                    name="user"
                    style={Styles.iconForm}
                  />
                  <View style={Styles.verticalLine} />
                  <Text style={{fontFamily: 'segoeui', marginStart: 5}}>
                    {this.state.dataUser.NAMA}
                  </Text>
                </View>
                <View style={Styles.textIcon}>
                  <Icon
                    type="SimpleLineIcons"
                    name="graduation"
                    style={Styles.iconForm}
                  />
                  <View style={Styles.verticalLine} />
                  <Text style={{fontFamily: 'segoeui', marginStart: 5}}>
                    {this.state.dataUser.NIM}
                  </Text>
                </View>
              </View>

              <View style={{justifyContent: 'flex-end'}}>
                <TouchableOpacity
                  onPress={() =>
                    this.state.saldoContainer == true
                      ? this.setState({saldoContainer: false})
                      : this.setState({saldoContainer: true})
                  }>
                  <Icon
                    type="SimpleLineIcons"
                    name={arrow}
                    style={Styles.iconForm}
                  />
                </TouchableOpacity>
              </View>
            </View>

            {this.state.saldoContainer == true ? this._renderSaldo() : null}
          </View>
        </ImageBackground>

        <View style={{flex: 1.5}}>{this._renderCamera()}</View>
      </Container>
    );
  }
}

const Styles = StyleSheet.create({
  headerContainer: {
    flexDirection: 'row',
    flex: 1,
    alignSelf: 'center',
  },
  logoText: {
    fontFamily: 'segoeui',
    fontWeight: 'bold',
    color: Colors.COLOR_WHITE,
  },
  mainContainer: {
    flexDirection: 'column',
    flex: 1,
  },
  mainHeader: {
    flexDirection: 'row',
    marginStart: deviceWidth * 0.02,
    marginEnd: deviceWidth * 0.02,
    marginTop: deviceWidth * 0.02,
    padding: deviceWidth * 0.02,
    borderTopLeftRadius: deviceWidth * 0.01,
    borderTopRightRadius: deviceWidth * 0.01,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.9,
    shadowRadius: deviceWidth * 0.01,
    elevation: 1,
    zIndex: 2,
    backgroundColor: Colors.COLOR_WHITE,
  },
  secondHeader: {
    flexDirection: 'column',
    marginStart: deviceWidth * 0.025,
    marginEnd: deviceWidth * 0.025,
    marginTop: deviceHeight * -0.02,
    padding: deviceWidth * 0.02,
    borderRadius: deviceWidth * 0.01,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.9,
    shadowRadius: deviceWidth * 0.01,
    elevation: 1,
    paddingTop: deviceHeight * 0.025,
    position: 'relative',
    zIndex: 1,
  },
  iconForm: {
    fontSize: 18,
    color: Colors.COLOR_BLACK_47,
    marginHorizontal: 10,
  },
  textIcon: {
    flexDirection: 'row',
  },
  verticalLine: {
    borderLeftWidth: 5,
    borderLeftColor: Colors.COLOR_PRIMARY,
  },
  poinContainer: {
    flexDirection: 'row',
  },
  curencyContainer: {
    flex: 1,
  },
  refreshContainer: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  iconRefresh: {
    fontSize: 30,
    margin: deviceWidth * 0.02,
    color: Colors.COLOR_WHITE,
  },
});

export default homeScreen;
