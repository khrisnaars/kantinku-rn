import React, {Component} from 'react';
import {
  Text,
  View,
  Container,
  Header,
  Body,
  Content,
  Right,
  Left,
  Icon,
  Toast,
  List,
  ListItem,
} from 'native-base';
import {
  StyleSheet,
  Image,
  Dimensions,
  Platform,
  ImageBackground,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {CommonActions} from '@react-navigation/native';
import axios from 'react-native-axios';
import Spinner from 'react-native-loading-spinner-overlay';
import CurrencyFormat from 'react-currency-format';
import moment from 'moment';

//DEVICE DIMENSION
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

//UTILS
import Logos from '../../utils/logos';
import Icons from '../../utils/icons';
import Colors from '../../utils/colors';
import GlobalVariables from '../../utils/globalVariables';
import {TouchableOpacity} from 'react-native-gesture-handler';

//RESET NAVIGATION
const resetLogin = CommonActions.reset({
  index: 1,
  routes: [{name: 'loginScreen'}],
});

//CUSTOM COMPONENT
import StatusBarPrimary from '../statusBar/statusBarPrimary';
import backgrounds from '../../utils/backgrounds';

class mutasiScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dataUser: [],
      loading: true,
      dataTransaksi: [],
    };
  }

  componentWillMount() {
    this.getData();
  }

  async getData() {
    var dataUser = await AsyncStorage.getItem('dataUser');

    this.setState(
      {
        dataUser: JSON.parse(dataUser),
      },
      () => this._getDataTransaksi(this.state.dataUser),
    );
  }

  async getDataTransaksi(data) {
    let url =
      GlobalVariables.URL_KANTINKU + GlobalVariables.URL_POST_DATA_MUTASI;

    let parameter = {
      nim: data.NIM,
    };

    const res = await axios.post(url, parameter);

    return await res;
  }

  _getDataTransaksi(data) {
    this.getDataTransaksi(data)
      .then(response => {
        response = response.data;
        if (response.status == 1) {
          this.setState({loading: false, dataTransaksi: response.data});
        } else {
          this.setState({loading: false}, () => {
            Toast.show({
              text: response.error,
              buttonText: 'X',
              type: 'danger',
              duration: 3000,
            });
          });
        }
      })
      .catch(function(error) {
        this.setState({loading: false}, () => {
          Toast.show({
            text:
              'Terjadi kesalahan pastikan koneksi anda baik, dan hubungi administrator',
            buttonText: 'X',
            type: 'danger',
            duration: 3000,
          });
        });
      });
  }

  renderRow(item) {
    return (
      <View style={Styles.mainHeader}>
        <View
          style={{
            flex: 1,
            margin: deviceWidth * 0.02,
            flexDirection: 'column',
          }}>
          <View style={Styles.textIcon}>
            <Icon type="SimpleLineIcons" name="clock" style={Styles.iconForm} />
            <View style={Styles.verticalLine} />
            <Text style={{fontFamily: 'segoeui', marginStart: 5}}>
              {moment(item.CREATED_DATE).format('DD/MM/YYYY hh:mm:ss')}
            </Text>
          </View>
          <View style={Styles.textIcon}>
            <Icon
              type="SimpleLineIcons"
              name="wallet"
              style={Styles.iconForm}
            />
            <View style={Styles.verticalLine} />
            <CurrencyFormat
              value={item.NOMINAL}
              displayType={'text'}
              thousandSeparator={true}
              renderText={value => (
                <Text style={{fontFamily: 'segoeui', marginStart: 5}}>
                  {item.STATUS + ' - ' + value}
                </Text>
              )}
            />
          </View>
          <View style={Styles.textIcon}>
            <Text style={{fontFamily: 'segoeui', marginStart: 5}}>
              {item.KETERANGAN}
            </Text>
          </View>
        </View>
      </View>
    );
  }

  render() {
    return (
      <Container>
        <Spinner
          visible={this.state.loading}
          textContent={'Loading...'}
          textStyle={{color: Colors.COLOR_BLACK}}
        />

        <ImageBackground
          source={backgrounds.background_kantinku_02}
          style={{width: deviceWidth, flex: 1}}>
          <List
            style={Styles.listContainer}
            dataArray={this.state.dataTransaksi}
            renderRow={item => this.renderRow(item)}
          />
        </ImageBackground>
      </Container>
    );
  }
}

const Styles = StyleSheet.create({
  headerContainer: {
    flexDirection: 'row',
    flex: 1,
    alignSelf: 'center',
  },
  logoText: {
    fontFamily: 'segoeui',
    fontWeight: 'bold',
    color: Colors.COLOR_WHITE,
  },
  mainHeader: {
    flexDirection: 'row',
    marginTop: deviceWidth * 0.02,
    borderRadius: deviceWidth * 0.01,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.9,
    shadowRadius: deviceWidth * 0.01,
    elevation: 1,
    backgroundColor: Colors.COLOR_WHITE,
  },
  iconForm: {
    fontSize: 18,
    color: Colors.COLOR_BLACK_47,
    marginHorizontal: 10,
  },
  textIcon: {
    flexDirection: 'row',
  },
  verticalLine: {
    borderLeftWidth: 5,
    borderLeftColor: Colors.COLOR_PRIMARY,
  },
  listContainer: {
    flex: 1,
    margin: deviceWidth * 0.02,
  },
});

export default mutasiScreen;
