import React, {Component} from 'react';
import {
  Text,
  View,
  Container,
  Header,
  Body,
  Content,
  Right,
  Left,
  Icon,
  Toast,
  List,
  Card,
  ListItem,
  Button,
  CardItem,
} from 'native-base';
import {
  StyleSheet,
  Image,
  Dimensions,
  Platform,
  ImageBackground,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {CommonActions} from '@react-navigation/native';
import axios from 'react-native-axios';
import Spinner from 'react-native-loading-spinner-overlay';
import CurrencyFormat from 'react-currency-format';
import moment from 'moment';

//DEVICE DIMENSION
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

//UTILS
import Logos from '../../utils/logos';
import Icons from '../../utils/icons';
import Colors from '../../utils/colors';
import GlobalVariables from '../../utils/globalVariables';
import {
  TouchableOpacity,
  TextInput,
  ScrollView,
} from 'react-native-gesture-handler';

//RESET NAVIGATION
const resetLogin = CommonActions.reset({
  index: 1,
  routes: [{name: 'loginScreen'}],
});

//CUSTOM COMPONENT
import StatusBarPrimary from '../statusBar/statusBarPrimary';
import backgrounds from '../../utils/backgrounds';
import {block} from 'react-native-reanimated';

class detailHistoryScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      dataDetail: [],
      dataTrans: this.props.route.params.dataTrans,
      dataUser: [],
      section: this.props.route.params.section,
    };
  }

  componentWillMount() {
    this.getData();
  }

  async getData() {
    var dataUser = await AsyncStorage.getItem('dataUser');

    this.setState(
      {
        dataUser: JSON.parse(dataUser),
      },
      () => this._getDataTransaksi(this.state.dataTrans),
    );
  }

  async getDataTransaksi(data) {
    let url =
      GlobalVariables.URL_KANTINKU +
      GlobalVariables.URL_POST_DATA_DETAIL_HISTORY_USER;

    let parameter = {
      transaksi_h_id: data.ID,
    };

    const res = await axios.post(url, parameter);

    return await res;
  }

  _getDataTransaksi(data) {
    this.getDataTransaksi(data)
      .then(response => {
        response = response.data;
        if (response.status == 1) {
          this.setState({loading: false, dataDetail: response.data});
        } else {
          this.setState({loading: false}, () => {
            Toast.show({
              text: response.error,
              buttonText: 'X',
              type: 'danger',
              duration: 3000,
            });
          });
        }
      })
      .catch(function(error) {
        this.setState({loading: false}, () => {
          Toast.show({
            text:
              'Terjadi kesalahan pastikan koneksi anda baik, dan hubungi administrator',
            buttonText: 'X',
            type: 'danger',
            duration: 3000,
          });
        });
      });
  }

  render() {
    return (
      <Container>
        <Spinner
          visible={this.state.loading}
          textContent={'Loading...'}
          textStyle={{color: Colors.COLOR_BLACK}}
        />

        <ImageBackground
          source={backgrounds.background_kantinku_02}
          style={{width: deviceWidth, flex: 1, justifyContent: 'center'}}>
          <Card
            style={{
              width: deviceWidth * 0.95,
              borderRadius: 20,
              height: deviceHeight * 0.8,
              alignSelf: 'center',
              display: 'flex',
              backgroundColor: Colors.COLOR_BLACK_97,
            }}>
            <CardItem
              style={{
                height: deviceHeight * 0.1,
                flexDirection: 'column',
                borderTopEndRadius: 20,
                borderTopStartRadius: 20,
                backgroundColor: Colors.COLOR_PRIMARY,
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontFamily: 'segoeui',
                  fontSize: 20,
                  color: Colors.COLOR_WHITE,
                }}>
                {this.state.dataTrans.ID}
              </Text>
            </CardItem>
            <CardItem style={{backgroundColor: Colors.COLOR_BLACK_97}}>
              <View style={{flexDirection: 'column'}}>
                <Text style={{fontSize: 18, fontFamily: 'segoeui'}}>
                  {this.state.dataTrans.NAMA_KIOS}
                </Text>
                <Text style={{fontSize: 18, fontFamily: 'segoeui'}}>
                  {moment(this.state.dataTrans.CREATED_DATE).format(
                    'DD/MM/YYYY - hh:mm:ss',
                  )}
                </Text>
              </View>
            </CardItem>
            <CardItem style={{backgroundColor: Colors.COLOR_BLACK_97}}>
              <View style={{flexDirection: 'column'}}>
                <Text style={{fontSize: 18, fontFamily: 'segoeui'}}>
                  {this.state.section == 1
                    ? this.state.dataTrans.pemesan.NAMA
                    : this.state.dataUser.NAMA}
                </Text>
                <Text style={{fontSize: 18, fontFamily: 'segoeui'}}>
                  {this.state.section == 1
                    ? this.state.dataTrans.NIM
                    : this.state.dataUser.NIM}
                </Text>
                <Text style={{fontSize: 16, fontFamily: 'segoeui'}}>
                  Keterangan : {this.state.dataTrans.KETERANGAN}
                </Text>
              </View>
            </CardItem>

            <ScrollView>
              {this.state.dataDetail.map(item => {
                return (
                  <CardItem
                    style={{
                      backgroundColor: Colors.COLOR_WHITE,
                      margin: deviceWidth * 0.02,
                    }}>
                    <View
                      style={{
                        backgroundColor: Colors.COLOR_WHITE,
                        margin: deviceWidth * 0.02,
                        padding: deviceWidth * 0.01,
                        flex: 1,
                      }}>
                      <Text style={{fontFamily: 'segoeui', fontSize: 20}}>
                        {item.NAMA}
                      </Text>

                      <View style={{display: 'flex', flexDirection: 'row'}}>
                        <View style={{display: 'flex', flex: 2}}>
                          <Text style={{fontFamily: 'segoeui', fontSize: 12}}>
                            {item.JNS_MENU}
                          </Text>

                          <CurrencyFormat
                            value={item.HARGA}
                            displayType={'text'}
                            thousandSeparator={true}
                            renderText={value => (
                              <Text
                                style={{fontFamily: 'segoeui', fontSize: 18}}>
                                {value}
                              </Text>
                            )}
                          />
                        </View>
                        <View
                          style={{
                            display: 'flex',
                            flex: 2,
                            backgroundColor: Colors.COLOR_PRIMARY,
                            justifyContent: 'center',
                            paddingHorizontal: deviceWidth * 0.01,
                          }}>
                          <CurrencyFormat
                            value={parseInt(item.JUMLAH) * parseInt(item.HARGA)}
                            displayType={'text'}
                            thousandSeparator={true}
                            renderText={value => (
                              <Text
                                style={{
                                  fontSize: 20,
                                  fontFamily: 'segoeui',
                                  color: Colors.COLOR_WHITE,
                                }}>
                                {item.JUMLAH + ' | ' + value}
                              </Text>
                            )}
                          />
                        </View>
                      </View>
                    </View>
                  </CardItem>
                );
              })}
            </ScrollView>
            <CardItem
              style={{
                borderTopColor: Colors.COLOR_BLACK,
                borderTopWidth: 1,
                borderBottomEndRadius: 20,
                borderBottomStartRadius: 20,
              }}>
              <View
                style={{
                  backgroundColor: Colors.COLOR_WHITE,
                  flex: 1,
                  justifyContent: 'center',
                }}>
                <View
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    padding: deviceWidth * 0.02,
                  }}>
                  <View style={{display: 'flex'}}>
                    <Text style={{fontFamily: 'segoeui', textAlign: 'right'}}>
                      Jumlah :{' '}
                    </Text>
                    <Text style={{fontFamily: 'segoeui', textAlign: 'right'}}>
                      Total :{' '}
                    </Text>
                  </View>
                  <View
                    style={{
                      display: 'flex',
                      flex: 1,
                      marginHorizontal: deviceWidth * 0.02,
                    }}>
                    <Text style={{fontFamily: 'segoeui', textAlign: 'right'}}>
                      {this.state.dataTrans.JUMLAH_TOTAL}
                    </Text>
                    <CurrencyFormat
                      value={this.state.dataTrans.GRAND_TOTAL}
                      displayType={'text'}
                      thousandSeparator={true}
                      renderText={value => (
                        <Text
                          style={{fontFamily: 'segoeui', textAlign: 'right'}}>
                          {value}
                        </Text>
                      )}
                    />
                  </View>
                  <View
                    style={{
                      display: 'flex',
                      flex: 2,
                      backgroundColor:
                        this.state.dataTrans.STATUS == 3
                          ? Colors.COLOR_RED
                          : Colors.COLOR_GREEN,
                      justifyContent: 'center',
                    }}>
                    <Text
                      style={{
                        color: Colors.COLOR_WHITE,
                        textAlign: 'center',
                        fontFamily: 'segoeui',
                        fontWeight: 'bold',
                      }}>
                      {this.state.dataTrans.STATUS == 1
                        ? 'Verified'
                        : this.state.dataTrans.STATUS == 2
                        ? 'Success'
                        : 'Rejected'}
                    </Text>
                  </View>
                </View>
              </View>
            </CardItem>
          </Card>
        </ImageBackground>
      </Container>
    );
  }
}

const Styles = StyleSheet.create({
  headerContainer: {
    flexDirection: 'row',
    flex: 1,
    alignSelf: 'center',
  },
  logoText: {
    fontFamily: 'segoeui',
    fontWeight: 'bold',
    color: Colors.COLOR_WHITE,
  },
  mainHeader: {
    flexDirection: 'row',
    marginTop: deviceWidth * 0.02,
    padding: deviceWidth * 0.02,
    borderRadius: deviceWidth * 0.01,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.9,
    shadowRadius: deviceWidth * 0.01,
    elevation: 1,
    zIndex: 2,
    backgroundColor: Colors.COLOR_WHITE,
  },
  iconForm: {
    fontSize: 18,
    color: Colors.COLOR_BLACK_47,
    marginHorizontal: 10,
  },
  textIcon: {
    flexDirection: 'row',
  },
  verticalLine: {
    borderLeftWidth: 5,
    borderLeftColor: Colors.COLOR_PRIMARY,
  },
  listContainer: {
    flex: 1,
    margin: deviceWidth * 0.02,
  },
});

export default detailHistoryScreen;
