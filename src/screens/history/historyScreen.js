import React, {Component} from 'react';
import {
  Text,
  View,
  Container,
  Header,
  Body,
  Content,
  Right,
  Left,
  Icon,
  Toast,
  List,
  Input,
  ListItem,
  Button,
} from 'native-base';
import {
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  Platform,
  ImageBackground,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {CommonActions} from '@react-navigation/native';
import axios from 'react-native-axios';
import Spinner from 'react-native-loading-spinner-overlay';
import CurrencyFormat from 'react-currency-format';
import moment from 'moment';
import Modal from 'react-native-modal';
import DatePicker from 'react-native-datepicker';
import {Picker} from '@react-native-community/picker';

//DEVICE DIMENSION
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

//UTILS
import Logos from '../../utils/logos';
import Icons from '../../utils/icons';
import Colors from '../../utils/colors';
import GlobalVariables from '../../utils/globalVariables';

//RESET NAVIGATION
const resetLogin = CommonActions.reset({
  index: 1,
  routes: [{name: 'loginScreen'}],
});

//CUSTOM COMPONENT
import StatusBarPrimary from '../statusBar/statusBarPrimary';
import backgrounds from '../../utils/backgrounds';

class historyScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dataUser: [],
      loading: true,
      section: this.props.route.params.section,
      dataTransaksi: [],
      modalFilter: false,
      totalHarga: 0,
      totalJumlah: 0,
      jenisTransaksi: -1,
      tanggalAwal: moment().format('DD/MM/YYYY'),
      tanggalAkhir: moment().format('DD/MM/YYYY'),
    };
  }

  componentWillMount() {
    this.getData();
    this.state.section == 1 ? this.countTrans() : null;
  }

  async getData() {
    var dataUser = await AsyncStorage.getItem('dataUser');

    this.setState(
      {
        dataUser: JSON.parse(dataUser),
      },
      () => this._getDataTransaksi(this.state.dataUser),
    );
  }

  async getDataTransaksi(data) {
    let url =
      GlobalVariables.URL_KANTINKU +
      (this.state.section == 1
        ? GlobalVariables.URL_POST_DATA_HISTORY_KIOS
        : GlobalVariables.URL_POST_DATA_HISTORY_USER);

    let parameter = [];

    if (this.state.section == 1) {
      parameter = {
        kios_id: data.ID,
        transaksi: this.state.jenisTransaksi,
        tanggalAwal: moment(this.state.tanggalAwal,'DD/MM/YYYY').format('YYYY-MM-DD'),
        tanggalAkhir: moment(this.state.tanggalAkhir,'DD/MM/YYYY').add(1, 'days').format('YYYY-MM-DD'),
      };
    } else {
      parameter = {
        nim: data.NIM,
      };
    }

    const res = await axios.post(url, parameter);

    return await res;
  }

  _getDataTransaksi(data) {
    this.getDataTransaksi(data)
      .then(response => {
        response = response.data;
        if (response.status == 1) {
          this.setState({loading: false, dataTransaksi: response.data}, () => {
            this.state.section == 1 ? this.countTrans() : null;
          });
        } else {
          this.setState({loading: false}, () => {
            Toast.show({
              text: response.error,
              buttonText: 'X',
              type: 'danger',
              duration: 3000,
            });
          });
        }
      })
      .catch(function(error) {
        this.setState({loading: false}, () => {
          Toast.show({
            text:
              'Terjadi kesalahan pastikan koneksi anda baik, dan hubungi administrator',
            buttonText: 'X',
            type: 'danger',
            duration: 3000,
          });
        });
      });
  }

  _getDetailHistory(dataTrans) {
    this.props.navigation.navigate('detailHistoryScreen', {
      dataTrans: dataTrans,
      section: this.state.section == 1 ? 1 : 0,
    });
  }

  renderRow(item) {
    return (
      <TouchableOpacity onPress={() => this._getDetailHistory(item)}>
        <View style={Styles.mainHeader}>
          <View style={{flex: 1, justifyContent: 'center'}}>
            <Text
              style={{fontFamily: 'segoeui', fontWeight: 'bold', fontSize: 25}}>
              {this.state.section == 1 ? item.pemesan.NAMA : item.NAMA_KIOS}
            </Text>
            <Text style={{fontFamily: 'segoeui'}}>{item.ID}</Text>
            <View
              style={{
                flexDirection: 'row',
                paddingVertical: deviceWidth * 0.02,
                borderBottomColor: Colors.COLOR_BLACK,
                borderBottomWidth: 1,
              }}>
              <Text style={{fontFamily: 'segoeui'}}>
                {moment(item.CREATED_DATE).format('DD/MM/YYYY - hh:mm:ss')}
              </Text>
              <Text style={{marginStart: deviceWidth * 0.02}}>{'\u2B24'}</Text>
              <View
                style={{
                  backgroundColor:
                    item.STATUS == 3 ? Colors.COLOR_RED : Colors.COLOR_GREEN,
                  padding: deviceWidth * 0.008,
                  borderRadius: 5,
                  marginStart: deviceWidth * 0.02,
                }}>
                <Text
                  style={{
                    fontFamily: 'segoeui',
                    fontWeight: 'bold',
                    color: Colors.COLOR_WHITE,
                  }}>
                  {item.STATUS == 1
                    ? 'Verified'
                    : item.STATUS == 2
                    ? 'Success'
                    : 'Rejected'}
                </Text>
              </View>
            </View>
            <CurrencyFormat
              value={item.GRAND_TOTAL}
              displayType={'text'}
              thousandSeparator={true}
              renderText={value => (
                <Text
                  style={{
                    textAlign: 'right',
                    fontFamily: 'segoeui',
                    fontSize: 18,
                  }}>
                  {item.JUMLAH_TOTAL + ' | ' + value}
                </Text>
              )}
            />
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  countTrans() {
    var totalJumlah = 0;
    var totalHarga = 0;

    this.state.dataTransaksi.map(item => {
      totalJumlah = totalJumlah + item.JUMLAH_TOTAL;
      totalHarga = totalHarga + item.GRAND_TOTAL;
    });

    this.setState({
      totalJumlah: totalJumlah,
      totalHarga: totalHarga,
    });
  }

  filter() {
    this.setState(
      {
        loading: true,
        modalFilter: false,
      },
      () => {
        this._getDataTransaksi(this.state.dataUser);
      },
    );
  }

  _modalFilter() {
    return (
      <Modal isVisible={this.state.modalFilter}>
        <View
          style={{
            borderRadius: deviceWidth * 0.01,
            alignSelf: 'center',
            width: deviceWidth * 0.9,
            height: deviceHeight * 0.3,
            backgroundColor: Colors.COLOR_WHITE,
          }}>
          <Text
            style={{
              margin: deviceWidth * 0.02,
              fontFamily: 'segoeui',
              fontWeight: 'bold',
            }}>
            Periode Transaksi :{' '}
          </Text>
          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
              margin: deviceWidth * 0.02,
            }}>
            <DatePicker
              style={{textAlign: 'center', flex: 1}}
              date={this.state.tanggalAwal}
              mode={'date'}
              maxDate={this.state.tanggalAkhir}
              showIcon={false}
              format={'DD/MM/YYYY'}
              display="default"
              onDateChange={date => {
                this.setState({tanggalAwal: date});
              }}
            />
            <DatePicker
              style={{textAlign: 'center', flex: 1}}
              date={this.state.tanggalAkhir}
              mode={'date'}
              maxDate={moment().format('DD/MM/YYYY')}
              format={'DD/MM/YYYY'}
              showIcon={false}
              display="default"
              onDateChange={date => {
                this.setState({tanggalAkhir: date});
              }}
            />
          </View>
          <Text
            style={{
              margin: deviceWidth * 0.02,
              fontFamily: 'segoeui',
              fontWeight: 'bold',
            }}>
            Jenis Transaksi :{' '}
          </Text>
          <Picker
            selectedValue={this.state.jenisTransaksi}
            onValueChange={itemValue =>
              this.setState({jenisTransaksi: itemValue})
            }>
            <Picker.Item label="All" value="-1" />
            <Picker.Item label="Success" value="2" />
            <Picker.Item label="Verfied" value="1" />
            <Picker.Item label="Rejected" value="3" />
          </Picker>
          <Button
            style={{
              justifyContent: 'center',
              backgroundColor: Colors.COLOR_GREEN,
              borderBottomEndRadius: 0,
              borderBottomstartRadius: 0,
            }}
            onPress={() => this.filter()}>
            <Text style={{fontFamily: 'segoeui', fontWeight: 'bold'}}>
              Filter
            </Text>
          </Button>
        </View>
      </Modal>
    );
  }

  _renderBottom() {
    return (
      <View
        style={{
          backgroundColor: Colors.COLOR_WHITE,
          height: deviceHeight * 0.1,
          justifyContent: 'center',
        }}>
        <View
          style={{
            display: 'flex',
            flexDirection: 'row',
            padding: deviceWidth * 0.02,
          }}>
          <View style={{display: 'flex'}}>
            <Text style={{fontFamily: 'segoeui', textAlign: 'right'}}>
              Jumlah :{' '}
            </Text>
            <Text style={{fontFamily: 'segoeui', textAlign: 'right'}}>
              Total :{' '}
            </Text>
          </View>
          <View
            style={{
              display: 'flex',
              flex: 1,
              marginHorizontal: deviceWidth * 0.02,
            }}>
            <Text style={{fontFamily: 'segoeui', textAlign: 'right'}}>
              {this.state.totalJumlah}
            </Text>
            <CurrencyFormat
              value={this.state.totalHarga}
              displayType={'text'}
              thousandSeparator={true}
              renderText={value => (
                <Text style={{fontFamily: 'segoeui', textAlign: 'right'}}>
                  {value}
                </Text>
              )}
            />
          </View>
          <TouchableOpacity
            onPress={() => this.setState({modalFilter: true})}
            style={{
              display: 'flex',
              flex: 2,
              backgroundColor: Colors.COLOR_GREEN,
              justifyContent: 'center',
            }}>
            <Text
              style={{
                color: Colors.COLOR_WHITE,
                textAlign: 'center',
                fontFamily: 'segoeui',
                fontWeight: 'bold',
                margin: deviceWidth * 0.02,
              }}>
              FILTER
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  render() {
    return (
      <Container>
        <Spinner
          visible={this.state.loading}
          textContent={'Loading...'}
          textStyle={{color: Colors.COLOR_BLACK}}
        />
        {this._modalFilter()}
        <ImageBackground
          source={backgrounds.background_kantinku_02}
          style={{width: deviceWidth, flex: 1}}>
          <List
            keyExtractor={item => item.ID}
            style={Styles.listContainer}
            dataArray={this.state.dataTransaksi}
            renderRow={item => this.renderRow(item)}
          />

          {this.state.section == 1 ? this._renderBottom() : null}
        </ImageBackground>
      </Container>
    );
  }
}

const Styles = StyleSheet.create({
  headerContainer: {
    flexDirection: 'row',
    flex: 1,
    alignSelf: 'center',
  },
  logoText: {
    fontFamily: 'segoeui',
    fontWeight: 'bold',
    color: Colors.COLOR_WHITE,
  },
  mainHeader: {
    flexDirection: 'row',
    marginTop: deviceWidth * 0.02,
    padding: deviceWidth * 0.02,
    borderRadius: deviceWidth * 0.01,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.9,
    shadowRadius: deviceWidth * 0.01,
    elevation: 1,
    zIndex: 2,
    backgroundColor: Colors.COLOR_WHITE,
  },
  iconForm: {
    fontSize: 18,
    color: Colors.COLOR_BLACK_47,
    marginHorizontal: 10,
  },
  textIcon: {
    flexDirection: 'row',
  },
  verticalLine: {
    borderLeftWidth: 5,
    borderLeftColor: Colors.COLOR_PRIMARY,
  },
  listContainer: {
    flex: 1,
    margin: deviceWidth * 0.02,
  },
});

export default historyScreen;
