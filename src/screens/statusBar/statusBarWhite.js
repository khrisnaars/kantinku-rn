import React from 'react';
import {StatusBar} from 'react-native';

//UTILS
import Colors from '../../utils/colors';

function StatusBarWhite() {
  return (
    <StatusBar backgroundColor={Colors.COLOR_WHITE} barStyle={'dark-content'} />
  );
}

export default StatusBarWhite;
