import React from 'react';
import {StatusBar} from 'react-native';

//UTILS
import Colors from '../../utils/colors';

function StatusBarBlackOPC05() {
  return (
    <StatusBar
      backgroundColor={Colors.COLOR_BLACK_OPC_05}
      barStyle={'dark-content'}
    />
  );
}

export default StatusBarBlackOPC05;
