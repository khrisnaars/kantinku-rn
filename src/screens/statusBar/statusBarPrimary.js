import React from 'react';
import {StatusBar} from 'react-native';

//UTILS
import Colors from '../../utils/colors';

function StatusBarPrimary() {
  return (
    <StatusBar
      backgroundColor={Colors.COLOR_PRIMARY}
      barStyle={'dark-content'}
    />
  );
}

export default StatusBarPrimary;
