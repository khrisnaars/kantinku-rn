const backgrounds = {
  background_kantinku_01: require('../../assets/backgrounds/background-kantinku-01.png'),
  background_kantinku_02: require('../../assets/backgrounds/background-kantinku-02.png'),
};

export default backgrounds;
