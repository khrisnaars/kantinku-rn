export default {
  //Header URL
  URL_KANTINKU: 'https://kantinku-undika.000webhostapp.com/api/',

  //Post URL
  URL_POST_LOGIN: 'user/login',
  URL_POST_CEKLOGIN: 'user/cekAccount',
  URL_POST_GETSALDO: 'user/saldo',
  URL_POST_DATA_MUTASI: 'user/mutasi',
  URL_POST_DATA_HISTORY_USER: 'user/history',
  URL_POST_DATA_HISTORY_KIOS: 'kios/history',
  URL_POST_DATA_DETAIL_HISTORY_USER: 'user/detailhistory',
  URL_POST_BAYAR: 'user/bayar',
  URL_POST_TERIMA: 'kios/terima',
  URL_POST_TOLAK: 'kios/tolak',
  URL_POST_LOGIN_KANTIN: 'kios/login',
  URL_POST_DATA_TRANSAKSI: 'kios/dataTransaksi',
  URL_POST_MENU: 'kios/menu',
  URL_POST_UPDATE_STATUS_MENU: 'kios/status',
  URL_POST_EDIT_MENU: 'kios/editMenu',
  URL_POST_STORE_MENU: 'kios/storeMenu',
  URL_POST_HAPUS_MENU: 'kios/hapusMenu',

  //Get URL

  //Header APP
};
