const icons = {
    icon_inabikers_01: require('../../assets/icons/icon-inabikers-01.png'),
    icon_inabikers_02: require('../../assets/icons/icon-inabikers-02.png'),
    icon_inabikers_03: require('../../assets/icons/icon-inabikers-03.png'),
    icon_inabikers_04: require('../../assets/icons/icon-inabikers-04.png'),
    icon_inabikers_05: require('../../assets/icons/icon-inabikers-05.png'),
  };
  
  export default icons;
  